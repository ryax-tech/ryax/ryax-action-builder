# Ryax Action Builder

This repository is part of [Ryax](https://github.com/RyaxTech/ryax).

It builds the container images for the Ryax user actions. Then it pushes them
to the internal registry with the name `<ACTION_ID>:<ACTION_VERSION>`.

## Requirements

Assume that you have following dependencies installed on your system :

* Python 3.8+ `Installation Guide <https://wiki.python.org/moin/BeginnersGuide/Download>`_
* Poetry (Dependency management and packaging tool) `Installation Guide <https://python-poetry.org/docs/#installation>`_
* Docker (Containerization Management) `Install Guide <https://docs.docker.com/get-docker/>`_
* Nix (Need to reference installation documentation)

## Quick start

1. Open terminal in project and run ``docker-compose up`` to start development services.
2. Open other terminal in project and run ``poetry install`` to install dependencies.
3. Run ``poetry shell`` to activate the virtualenv.
4. Run ``export $(grep -v '^#' .env | xargs -0)`` to load environment definition from file.
5. Run ``./ryax-action-builder`` to start application.

## Code Lint

To check lint rules, run following command :

```bash
poetry run ./lint.sh
```

Use the `-f` option to apply the autoformat:

```bash
poetry run ./lint.sh -f
```

## Pre Commit


It is recommended to create a pre-commit hook on git and make it executable:

```bash
cp ./pre-commit.sh ./.git/hooks/pre-commit
chmod +x ./.git/hooks/pre-commit
```

## Message compilation (Protobuff)

Use the following command to compile protobuf message definition (.proto file):

```bash
python -m grpc_tools.protoc  --mypy_out=. -I./ --python_out=. ./ryax/action_builder/infrastructure/messaging/messages/*.proto
```

```bash
python -m grpc_tools.protoc  \
  --mypy_out=. \
  -I. \
  --python_out=. \
  --grpc_python_out=. \
  --mypy_grpc_out=. \
  ./ryax/action_builder/infrastructure/messaging/messages/grpc_messages.proto
```

## Update wrapper

To update wrapper first check if your last modification of ryax-action-wrapper is
already merged on master. Then update with:

```shell
nix --extra-experimental-features nix-command flake --extra-experimental-features flakes lock --update-input actionWrappers
```

After you need to commit, push, merge so the CI will create a new builder referencing
the wrapper.
