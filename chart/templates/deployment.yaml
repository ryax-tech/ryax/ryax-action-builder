apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "action-builder.fullname" . }}
  labels: {{- include "action-builder.labels" . | nindent 4 }}
spec:
  replicas: 1
  {{- if .Values.persistence.enabled }}
  strategy:
    type: Recreate
  {{- end }}
  selector:
    matchLabels:
      {{- include "action-builder.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "action-builder.labels" . | nindent 8 }}
        app.kubernetes.io/component: "internal"
        ryax.tech/resource-type: "internal"
    spec:
      priorityClassName: {{ .Values.priorityClass }}
      initContainers:
        - name: {{ include "action-builder.fullname" . }}-pull-wrappers
          image: {{ printf "%s:%s" .Values.image.name .Values.image.tag }}
          imagePullPolicy: {{ .Values.imagePullPolicy }}
          command:
            - sh
            - -c
            - |
              echo Loading Ryax Wrappers from Gitlab...
              # Use an src directory to avoid git issues with ownership
              cd /data
              (git clone https://gitlab.com/ryax-tech/ryax/ryax-action-wrappers.git /tmp/wrappers/src && \
               cd /tmp/wrappers/src && git checkout $RYAX_WRAPPERS_GIT_REF \
               ) || (
               echo Unable to fetch Ryax Wrappers from Gitlab, fallback to the embedded version. && \
               cp -av /data/wrappers /tmp/wrappers/src)
              echo Done!
          env:
            - name: RYAX_WRAPPERS_GIT_REF
              value: {{ .Values.ryaxWrappersGitReference | default "NOT_EXISTING_REF" }}
          volumeMounts:
            - mountPath: "/tmp"
              name: "tmp-dir"
            - mountPath: "/var/tmp"
              name: "tmp-var-dir"
            - mountPath: "/tmp/wrappers"
              name: ryax-wrappers-code
        - name: {{ include "action-builder.fullname" . }}-persistent-nix-store
          image: {{ printf "%s:%s" .Values.image.name .Values.image.tag }}
          imagePullPolicy: {{ .Values.imagePullPolicy }}
          command:
            - sh
            - -c
            - |
              set -x
              set -e
              echo Copy /nix from the container image
              if [ ! -d /persist/nix/store ]
              then
                echo Bootstrapping the nix directory
                cp -av /nix/* /persist/nix
              else
                nix --extra-experimental-features nix-command copy --all --no-check-sigs --to /persist
              fi
              echo Done!
          volumeMounts:
            - mountPath: /persist/nix
              name: nix-store
          securityContext:
            runAsUser: 0
            runAsGroup: 0
      containers:
        - name: {{ include "action-builder.fullname" . }}
          image: {{ printf "%s:%s" .Values.image.name .Values.image.tag }}
          imagePullPolicy: {{ .Values.imagePullPolicy }}
          env:
            - name: RYAX_BROKER
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.brokerSecret }}
                  key: "broker"
            - name: RYAX_INTERNAL_REGISTRY
              value: {{ .Values.internalRegistry }}
            - name: RYAX_LOG_LEVEL
              value: {{ .Values.logLevel }}
            - name: RYAX_WRAPPERS_DIR
              value: /data/wrappers/src
            {{- if .Values.grpcAPI.enabled }}
            - name: RYAX_GRPC_API_ENABLED
              value: "1" # Leave empty to disable
            {{- end }}
            {{- if .Values.extraEnv }}
              {{- toYaml .Values.extraEnv | nindent 12 }}
            {{- end }}
          volumeMounts:
            - mountPath: "/tmp"
              name: "tmp-dir"
            - mountPath: "/var/tmp"
              name: "tmp-var-dir"
            - mountPath: "/data/wrappers"
              name: ryax-wrappers-code
            - mountPath: /nix
              name: nix-store
            {{- if .Values.actionRegistrySecret }}
            - mountPath: /home/ryax/.docker
              name: action-registry-creds
              readOnly: true
            {{- end }}
          livenessProbe:
            httpGet:
              path: "/healthz"
              port: 8080
            periodSeconds: 30
            failureThreshold: 1
          resources: {{- toYaml .Values.resources | nindent 12 }}
{{- if .Values.pullSecret }}
      imagePullSecrets:
        - name: {{ .Values.pullSecret }}
{{- end }}
      volumes:
        - emptyDir:
          name: "tmp-dir"
        - emptyDir:
          name: "tmp-var-dir"
        - emptyDir:
          name: ryax-wrappers-code
        {{- if .Values.persistence.enabled }}
        - name: nix-store
          persistentVolumeClaim:
            claimName: {{ include "action-builder.fullname" . }}-nix-store
        {{- else }}
        - emptyDir:
            sizeLimit: {{ .Values.nix.storeSize }}
          name: nix-store
        {{- end }}
        {{- if .Values.actionRegistrySecret }}
        - name: action-registry-creds
          secret:
            secretName: {{ .Values.actionRegistrySecret }}
            items:
              - key: .dockerconfigjson
                path: config.json
        {{- end }}

