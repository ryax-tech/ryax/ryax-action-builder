{
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";
  inputs.nix2containerInput.url = "github:nlewo/nix2container";
  inputs.actionWrappers = {
    # Use this to use a dev branch + nix flake update
    #url = "git+https://gitlab.com/ryax-tech/ryax/ryax-action-wrappers.git?ref=fix-patch-hach-changes";
    url = "git+https://gitlab.com/ryax-tech/ryax/ryax-action-wrappers.git";
  };
  inputs.flakeUtils.follows = "nix2containerInput/flake-utils";

  outputs = { self, nixpkgs, nix2containerInput, actionWrappers, flakeUtils }:
    let
      # Put the name of your service here
      myTool = "ryax-action-builder";

      buildDir = "/tmp/ryax/${myTool}";
      appDir = builtins.path { path = ./.; name = myTool; };
    in
    flakeUtils.lib.eachSystem [ "aarch64-linux" "x86_64-linux" ]
      (system:
        let
          pkgs = import nixpkgs { inherit system; };
          nix2containerPkgs = nix2containerInput.packages.${system};
          python = pkgs.python3;
          lib = import ./nix/lib.nix { inherit pkgs python; };
          lintEnv = (pkgs.buildFHSUserEnv {
             name = "dev-shell";
             targetPkgs = pkgs: [ python pkgs.poetry pkgs.grpc-tools ];
             runScript = ''
               bash -c "poetry install && poetry run ./lint.sh $@"
             '';
          });
        in
        {
          devShell = (pkgs.buildFHSUserEnv {
            name = "dev-shell";
            LD_LIBRARY_PATH="${pkgs.lib.makeLibraryPath [pkgs.stdenv.cc.cc.lib]}";
            targetPkgs = pkgs: [ python pkgs.poetry pkgs.grpc-tools ];
          }).env;

          packages = {
            test = lib.test;
            lint = lintEnv;
            install = lib.install appDir buildDir;
            image = pkgs.callPackage ./nix/docker.nix {
              inherit appDir myTool python;
              depsDir = (/. + buildDir);
              nix2container = nix2containerPkgs.nix2container;
              ryaxModuleWrappers = actionWrappers;
            };
          };
          # Enable autoformat
          formatter = pkgs.nixpkgs-fmt;
        }
      );
}
