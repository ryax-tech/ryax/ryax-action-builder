#!/usr/bin/env bash

set -e
# For debug
# set -x

while getopts "fd:" opt; do
  case "$opt" in
    f)
      DO_CHECK_ONLY="false"
      ;;
    d)
      CHECK_DIR="$OPTARG"
      ;;
    ?)
      echo "script usage: $(basename $0) [-f] [-d directory]" >&2
      exit 1
      ;;
  esac
done

TO_CHECK_DIR=${CHECK_DIR:-"."}
CHECK_ONLY=${DO_CHECK_ONLY:-"true"}
RUFF_EXTRA_OPTS="--fix"

if [[ $CHECK_ONLY == "true" ]]
then
    BLACK_EXTRA_OPTS="--check --diff"
    RUFF_EXTRA_OPTS="--diff"
fi

echo "-- Checking for architecture errors"

if grep -qFrin "from ryax.action_builder.application." ryax/action_builder/infrastructure/
then
    echo -e "\e[31mWARNING\e[0m: Some infrastructure modules import application code:"
    grep -Frin --color "from ryax.action_builder.application." ryax/action_builder/infrastructure/
fi

if grep -qFrin "from ryax.action_builder.application." ryax/action_builder/domain
then
    echo -e "\e[31mWARNING\e[0m: Some domain modules import application code:"
    grep -Frin --color "from ryax.action_builder.application." ryax/action_builder/domain
fi

if grep -qFrin "from ryax.action_builder.infrastructure." ryax/action_builder/domain
then
    echo -e "\e[31mWARNING\e[0m: Some domain modules import infrastructure code:"
    grep -Frin --color "from ryax.action_builder.infrastructure." ryax/action_builder/domain
fi

# Detect cross-infrastructure includes, except towards utils.
for dir in $(ls -d  ryax/action_builder/infrastructure/*/)
do
    dir_name=$(basename $dir)
    if grep -Frin "from ryax.action_builder.infrastructure." $dir | grep -Fv "ryax.action_builder.infrastructure.utils" | grep -Fvq "ryax.action_builder.infrastructure.$dir_name"
    then
        echo -e "\e[31mWARNING\e[0m: There are cross-infrastructure includes in $dir_name:"
        grep -Frin "from ryax.action_builder.infrastructure." $dir | grep -Fv "ryax.action_builder.infrastructure.utils" | grep -Fv --color ryax.action_builder.infrastructure.$dir_name
    fi
done

echo "-- Checking python formating"
black $TO_CHECK_DIR --exclude ".venv|docs|ci|ryaxpkgs|result|.*pb2.py|.*pb2_grpc.pyi|.poetry" $BLACK_EXTRA_OPTS

echo "-- Running python static checking"
ruff check $TO_CHECK_DIR $RUFF_EXTRA_OPTS

echo "-- Checking type annotations"
mypy ./ryax --exclude '(?x)(/*pb2.py|^.venv*)'
