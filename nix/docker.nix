{ nix2container
, myTool
, pkgs
, python
, ryaxModuleWrappers
, appDir
, depsDir
, tag ? "latest"
}:
let
  user = "ryax";
  group = "ryax";
  uid = 1200;
  gid = 1200;
  uidStr = builtins.toString uid;
  gidStr = builtins.toString gid;
  userHome = "/home/ryax";

  inherit (pkgs) runCommand cacert coreutils;
  defaultConfig = runCommand "config" { } ''
    # add cachix config
    mkdir -p $out${userHome}/.config/nix/
    cat > $out${userHome}/.config/nix/nix.conf <<EOF
    experimental-features = nix-command flakes
    substituters = https://cache.nixos.org https://ryaxtech.cachix.org
    trusted-public-keys = cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY= ryaxtech.cachix.org-1:hfnbvb2rRHVzXOJ5kxZbshjmhtflrucH38CuU+UlXBQ=
    netrc-file = ${userHome}/.config/nix/netrc
    EOF

    cat > $out/${userHome}/.config/nix/netrc <<EOF
    machine ryaxtech.cachix.org password eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIwZmZhZjQ5My00MzQ2LTQzYTQtODVlZS02YzllYzhkMjU2OWQiLCJzY29wZXMiOiJjYWNoZSJ9.aV_zx6SHvzsjf05OvxovTUxipgCA6ygSwK6-oFMAqn0
    EOF

    mkdir -p $out/etc/ssl/certs/
    ln -s ${cacert}/etc/ssl/certs/ca-bundle.crt $out/etc/ssl/certs/ca-certificates.crt

    # Create temporary directories
    mkdir $out/tmp
    mkdir -p $out/var/tmp
    mkdir -p $out/var/cache/ccache

    mkdir -p $out/usr/bin
    ln -s ${coreutils}/bin/env $out/usr/bin/env

    # Fix localhost DNS resolution
    cat > $out/etc/nsswitch.conf <<EOF
    hosts: files dns
    EOF

    # create the root user
    mkdir -p $out/etc/pam.d
    echo "root:x:0:0:Ryax User:root:/bin/bash" > $out/etc/passwd
    echo "root:!x:::::::" > $out/etc/shadow
    echo "root:x:0:" > $out/etc/group
    echo "root:x::" > $out/etc/gshadow

    # Setup pam support
    cat >> $out/etc/pam.d/other <<EOF
    account sufficient pam_unix.so
    auth sufficient pam_rootok.so
    password requisite pam_unix.so nullok sha512
    session required pam_unix.so
    EOF
    touch $out/etc/login.defs

    # create the Ryax user
    echo "${user}:x:${uidStr}:${gidStr}:Ryax User:${userHome}:/bin/bash" >> $out/etc/passwd
    echo "${user}:!x:::::::" >> $out/etc/shadow
    echo "${group}:x:${gidStr}:" >> $out/etc/group
    echo "${group}:x::" >> $out/etc/gshadow

    mkdir -p $out${userHome}
  '';
  config = nix2container.buildLayer {
    perms = [
      {
        path = defaultConfig;
        regex = "/tmp";
        mode = "1777";
      }
      {
        path = defaultConfig;
        regex = "/var/tmp";
        mode = "1777";
      }
      {
        path = defaultConfig;
        regex = "/var/cache/ccache";
        mode = "0770";
        uid = uid;
        gid = gid;
        uname = user;
        gname = group;
      }
      {
        path = defaultConfig;
        regex = "/home/${user}";
        mode = "0744";
        uid = uid;
        gid = gid;
        uname = user;
        gname = group;
      }
    ];

    copyToRoot =
      let
        pythonWithPackages = python.withPackages (ps: with ps; [ pip setuptools ]);
      in
      [
        (pkgs.buildEnv {
          name = "root";
          paths = with pkgs; [ coreutils gitMinimal pythonWithPackages cachix nix bashInteractive findutils procps skopeo ];
          pathsToLink = [ "/bin" ];
        })
        defaultConfig
      ];
  };
  dependencies = nix2container.buildLayer {
    copyToRoot = runCommand "stack" { } ''
      set -x
      mkdir -p $out/data
      # Install python environment created by pip prior to this build
      if [ -d ${depsDir}/.env ]; then
        cp -r ${depsDir}/.env $out/data/.env
      fi
    '';
    reproducible = false;
  };
  app = nix2container.buildLayer {
    copyToRoot = runCommand "app" { } ''
      # Install the app
      mkdir -p $out/data
      cp -r ${appDir}/ryax $out/data/ryax

      mkdir -p $out/bin/
      cp ${appDir}/ryax-action-builder $out/bin/ryax-action-builder
    '';
  };
  wrappers = nix2container.buildLayer {
    copyToRoot = runCommand "wrapper" { } ''
      mkdir -p $out/data/wrappers
      cp -r ${ryaxModuleWrappers}/* $out/data/wrappers
    '';
  };
in
nix2container.buildImage {
  name = myTool;
  inherit tag;

  layers = [ config dependencies app wrappers ];

  # Add nix inside
  initializeNixDatabase = true;
  nixUid = uid;
  nixGid = gid;

  config.User = user;

  config.EntryPoint = [ myTool ];
  config.Env = [
    "NIX_PAGER=cat"
    "ENV=/etc/profile"
    "PYTHONPATH=/data/.env:/data"
    # Ryax Specific variables
    "RYAX_WRAPPERS_DIR=/data/wrappers"
    # For grpc missing libstdc++
    "LD_LIBRARY_PATH=${pkgs.lib.makeLibraryPath [pkgs.stdenv.cc.cc.lib]}"
  ];
  config.WorkingDir = "/data/wrappers";
  config.Labels = {
    "ryax.tech" = myTool;
  };
}
