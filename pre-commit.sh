#!/usr/bin/env bash

SCRIPTPATH="$(dirname $(realpath $0))/../.."
echo ${SCRIPTPATH}
CMD="${SCRIPTPATH}/lint.sh $@"

poetry run $CMD