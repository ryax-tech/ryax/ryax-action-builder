# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import asyncio
import logging
from typing import AsyncGenerator

from aiohttp import web

from ryax.action_builder.container import ApplicationContainer
from ryax.action_builder.domain.action_build.action_build_events import (
    NoActionBuildEvent,
)
from ryax.action_builder.infrastructure.api.setup import setup as api_setup
from ryax.action_builder.infrastructure.messaging.setup import setup as messaging_setup


def init() -> web.Application:
    """Init application"""
    container = ApplicationContainer()

    # Setup configuration from env variables
    container.config.broker_url.from_env("RYAX_BROKER", required=True)
    container.config.ryax_internal_registry.from_env(
        "RYAX_INTERNAL_REGISTRY", required=True
    )
    container.config.wrappers_dir.from_env("RYAX_WRAPPERS_DIR", required=True)
    container.config.extra_nix_args.from_env("RYAX_EXTRA_NIX_ARGS", default="")
    container.config.log_level.from_env("RYAX_LOG_LEVEL", "INFO")

    container.config.grpc_api.server_port.from_env(
        "RYAX_GRPC_API_SERVER_PORT", as_=int, default=8087
    )
    container.config.grpc_api.enabled.from_env(
        "RYAX_GRPC_API_ENABLED", as_=bool, default=False
    )

    container.config.nix.garbage_collect_period.from_env(
        "RYAX_NIX_GARBAGE_COLLECT_PERIOD", as_=int, default=600
    )
    container.config.nix.garbage_collect_threshold.from_env(
        "RYAX_NIX_GARBAGE_COLLECT_THRESHOLD", as_=float, default=0.80
    )
    container.config.api.port.from_env("RYAX_API_PORT", as_=int, default=8080)

    # Setup logging
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s.%(msecs)03d %(levelname)-7s %(name)-22s %(message)s",
        datefmt="%Y-%m-%d,%H:%M:%S",
    )
    str_level = container.config.log_level()
    numeric_level = getattr(logging, str_level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: %s" % str_level)
    logging.getLogger("ryax").setLevel(numeric_level)
    logger = logging.getLogger(__name__)
    logger.info("Logging level is set to %s" % str_level.upper())

    # Setup application
    app: web.Application = container.app()
    api_setup(app, container)
    app["container"] = container

    # Setup messaging consumer
    consumer = container.messaging_consumer()
    messaging_setup(consumer, container)

    return app


async def background_tasks(app: web.Application) -> AsyncGenerator:
    container: ApplicationContainer = app["container"]

    if container.config.grpc_api.enabled():
        app["grpc_server"] = asyncio.create_task(container.grpc_api_service().serve())

    logger = logging.getLogger(__name__)
    logger.info("Start periodic background tasks")

    async def periodic() -> None:
        while True:
            await asyncio.sleep(container.config.nix.garbage_collect_period())
            await container.builder_service().garbage_collect(
                container.config.nix.garbage_collect_threshold()
            )

    task = asyncio.create_task(periodic())

    yield

    task.cancel()
    if container.config.grpc_api.enabled():
        app["grpc_server"].cancel()
        await asyncio.wait([app["grpc_server"], task])


async def on_startup(app: web.Application) -> None:
    """Define hook on application startup"""
    container: ApplicationContainer = app["container"]
    await container.messaging_engine().connect()
    await container.messaging_consumer().start()
    await container.messaging_publisher().publish([NoActionBuildEvent()])


async def on_cleanup(app: web.Application) -> None:
    """Define hook on application startup"""
    container: ApplicationContainer = app["container"]
    await container.messaging_engine().disconnect()


def start() -> None:
    app: web.Application = init()
    app.on_startup.append(on_startup)
    app.on_cleanup.append(on_cleanup)
    app.cleanup_ctx.append(background_tasks)
    web.run_app(app, port=app["container"].config.api.port())
