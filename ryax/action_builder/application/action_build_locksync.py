# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging
import threading

logger = logging.getLogger(__name__)


class ActionBuildLockSyncService:
    def __init__(self) -> None:
        # Initialize the isBuilding flag to track if a build is in progress.
        self.isBuilding: bool = False
        # Create a threading.Lock to synchronize access to the isBuilding flag.
        self.lock = threading.Lock()

    def lock_build(self) -> bool:
        """
        Attempts to acquire the build lock. If another build is in progress
        (isBuilding is True), it returns False. Otherwise, it sets isBuilding
        to True and returns True.

        Returns:
            bool: True if the lock was successfully acquired, False otherwise.
        """
        with self.lock:
            if self.isBuilding:
                return False

            self.isBuilding = True
            return True

    def unlock_build(self) -> None:
        """
        Releases the build lock by setting isBuilding to False.
        This method is thread-safe and ensures no race conditions.
        """
        with self.lock:
            self.isBuilding = False
