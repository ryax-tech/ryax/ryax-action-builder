# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.


import abc
import logging
import asyncio
from typing import Optional, AsyncGenerator

from ryax.action_builder.application.action_build_locksync import (
    ActionBuildLockSyncService,
)
from ryax.action_builder.domain.action_build.action_build_events import (
    ActionBuildEvent,
    ActionDeletedEvent,
    ActionBuildStartEvent,
    ActionBuildSuccessEvent,
    ActionBuildCanceledEvent,
    ActionBuildErrorEvent,
    ActionCancelBuildEvent,
)
from ryax.action_builder.domain.action_build.action_build_exceptions import (
    BuildException,
    BuildErrorCode,
)
from ryax.action_builder.domain.action_build.action_build_factory import (
    ActionBuildFactory,
)
from ryax.action_builder.domain.action_build.action_build_processor import (
    IActionBuildProcessor,
)
from ryax.action_builder.domain.events.base_event import BaseEvent
from ryax.action_builder.domain.events.event_publisher import IEventPublisher

logger = logging.getLogger(__name__)


class ActionBuildService:
    def __init__(
        self,
        action_build_processor: IActionBuildProcessor,
        event_publisher: IEventPublisher,
        action_build_factory: ActionBuildFactory,
        action_build_locksync: ActionBuildLockSyncService,
    ):
        self.action_build_processor = action_build_processor
        self.factory = action_build_factory
        self.event_publisher = event_publisher
        self.task: Optional[asyncio.Task] = None
        self.action_build_locksync = action_build_locksync

    async def build(
        self, event: ActionBuildEvent
    ) -> AsyncGenerator[tuple[int, BaseEvent | bytes | None], None]:
        """
        Handling the execution of the actual build.
        Can throw build exceptions.
        Should not be called without checking action_build_locksync first
        """

        # Declare res
        lock_file = b""

        yield 0, ActionBuildStartEvent(action_id=event.action_id)

        try:
            action_build = self.factory.from_action_build_event(event)

            (
                lock_file,
                action_ready_event,
            ) = await self.action_build_processor.process_build(action_build)

            await self.event_publisher.publish([action_ready_event])

            yield 0, ActionBuildSuccessEvent(action_id=event.action_id)

        except BuildException as err:
            if err.code is BuildErrorCode.BuildCanceled:
                logger.info("Build canceled")
                yield 0, ActionBuildCanceledEvent(action_id=event.action_id)
            else:
                logger.exception("Build failed")
                yield 0, ActionBuildErrorEvent(
                    action_id=event.action_id,
                    code=err.code.value,
                    logs=err.logs,
                )
        except (Exception, KeyboardInterrupt) as err:
            logger.exception("Internal error")
            yield 0, ActionBuildErrorEvent(
                action_id=event.action_id,
                code=BuildErrorCode.ErrorWhileRunningProcess.value,
                logs=(
                    f"Internal error while building the action: {err.__class__.__name__}: {err}"
                    "This might be due to a lack of resources. Try to increase disk space or memory."
                ),
            )

        finally:
            yield 1, lock_file

    async def cancel_build(self, event: ActionCancelBuildEvent) -> None:
        logger.info("Canceling build for action %s", event.action_id)
        await self.action_build_processor.cancel_build()

        self.action_build_locksync.unlock_build()

    @abc.abstractmethod
    async def process_action_build(self, event: ActionBuildEvent) -> bytes | None:
        pass

    @abc.abstractmethod
    async def delete_action(self, event: ActionDeletedEvent) -> None:
        pass
