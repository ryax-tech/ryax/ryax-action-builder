# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging
import asyncio
from typing import Optional, cast

from ryax.action_builder.application.action_build_locksync import (
    ActionBuildLockSyncService,
)
from ryax.action_builder.application.action_build_service import ActionBuildService
from ryax.action_builder.domain.action_build.action_build_events import (
    ActionBuildEvent,
    ActionDeletedEvent,
    ActionCancelBuildEvent,
    ActionBuildCanceledEvent,
    ActionBuildErrorEvent,
)
from ryax.action_builder.domain.action_build.action_build_factory import (
    ActionBuildFactory,
)
from ryax.action_builder.domain.action_build.action_build_processor import (
    IActionBuildProcessor,
)
from ryax.action_builder.domain.events.base_event import BaseEvent
from ryax.action_builder.domain.events.event_publisher import IEventPublisher

logger = logging.getLogger(__name__)


class ActionBuildServiceBroker(ActionBuildService):
    def __init__(
        self,
        action_build_processor: IActionBuildProcessor,
        event_publisher: IEventPublisher,
        action_build_factory: ActionBuildFactory,
        action_build_locksync: ActionBuildLockSyncService,
    ):
        super().__init__(
            action_build_processor,
            event_publisher,
            action_build_factory,
            action_build_locksync,
        )

        self.action_build_processor = action_build_processor
        self.factory = action_build_factory
        self.event_publisher = event_publisher
        self.task: Optional[asyncio.Task] = None
        self.action_build_locksync = action_build_locksync

    async def process_action_build(self, event: ActionBuildEvent) -> bytes | None:
        if not self.action_build_locksync.lock_build():
            await self.event_publisher.publish(
                [
                    ActionBuildErrorEvent(
                        action_id=event.action_id,
                        # TODO: Create a domain enum class
                        code=6,
                        logs="A build is already occurring",
                    )
                ]
            )
            return None

        lock_file = None
        async for code, value in self.build(event):
            if code == 0:
                await self.event_publisher.publish([cast(BaseEvent, value)])
            if code == 1:
                lock_file = cast(bytes, value)

        self.action_build_locksync.unlock_build()

        return lock_file

    async def cancel_build(self, event: ActionCancelBuildEvent) -> None:
        logger.info("Canceling build for action %s", event.action_id)
        await self.action_build_processor.cancel_build()
        await self.event_publisher.publish(
            [ActionBuildCanceledEvent(action_id=event.action_id)]
        )

    async def delete_action(self, event: ActionDeletedEvent) -> None:
        await self.action_build_processor.cleanup(event.action_id, event.action_version)
