# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from aiohttp import web
from dependency_injector import providers
from dependency_injector.containers import DeclarativeContainer
from dependency_injector.ext import aiohttp

from ryax.action_builder.application.action_build_locksync import (
    ActionBuildLockSyncService,
)
from ryax.action_builder.application.action_build_service import ActionBuildService
from ryax.action_builder.application.action_build_service_broker import (
    ActionBuildServiceBroker,
)
from ryax.action_builder.application.util_service import UtilService
from ryax.action_builder.domain.action_build.action_build_factory import (
    ActionBuildFactory,
)
from ryax.action_builder.infrastructure.builder.builder_service import BuilderService
from ryax.action_builder.infrastructure.builder.external_commands import (
    ExternalCommandsService,
)
from ryax.action_builder.infrastructure.messaging.utils.consumer import (
    MessagingConsumer,
)
from ryax.action_builder.infrastructure.messaging.utils.engine import MessagingEngine
from ryax.action_builder.infrastructure.grpc_api.grpc_api import GRPCAPIService
from ryax.action_builder.infrastructure.messaging.utils.publisher import (
    MessagingPublisher,
)


class ApplicationContainer(DeclarativeContainer):
    """Application container for dependency injection"""

    # Define configuration providers
    config = providers.Configuration()

    # Define application provider
    app = aiohttp.Application(web.Application)

    # Broker services
    messaging_engine = providers.Singleton(
        MessagingEngine, connection_url=config.broker_url
    )
    messaging_consumer = providers.Singleton(MessagingConsumer, engine=messaging_engine)
    messaging_publisher = providers.Singleton(
        MessagingPublisher, engine=messaging_engine
    )

    external_commands_service = providers.Singleton(
        ExternalCommandsService,
        wrappers_dir=config.wrappers_dir,
        extra_nix_args=config.extra_nix_args,
    )

    # Builder services
    builder_service = providers.Singleton(
        BuilderService,
        internal_registry=config.ryax_internal_registry,
        external_commands_service=external_commands_service,
    )

    action_build_factory: providers.Singleton[ActionBuildFactory] = providers.Singleton(
        ActionBuildFactory,
    )

    action_build_locksync = providers.Singleton(ActionBuildLockSyncService)

    # Application services
    action_build_service = providers.Singleton(
        ActionBuildService,
        action_build_processor=builder_service,
        event_publisher=messaging_publisher,
        action_build_factory=action_build_factory,
        action_build_locksync=action_build_locksync,
    )

    action_build_service_broker = providers.Singleton(
        ActionBuildServiceBroker,
        action_build_processor=builder_service,
        event_publisher=messaging_publisher,
        action_build_factory=action_build_factory,
        action_build_locksync=action_build_locksync,
    )

    util_service = providers.Singleton(UtilService)

    grpc_api_service = providers.Singleton(
        GRPCAPIService,
        service=action_build_service,
        action_build_locksync=action_build_locksync,
        server_port=config.grpc_api.server_port,
    )
