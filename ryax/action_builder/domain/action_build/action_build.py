# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.


from dataclasses import dataclass

from ryax.action_builder.domain.action_build.action_build_values import (
    ActionKind,
    ActionType,
)


@dataclass
class ActionBuild:
    action_id: str
    kind: ActionKind
    technical_name: str
    wrapper_type_list: list[str]
    type: ActionType
    version: str
    archive: bytes
    project: str
    owner_id: str
