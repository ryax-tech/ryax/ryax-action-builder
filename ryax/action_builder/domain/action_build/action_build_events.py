# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import uuid

from typing import Union

from dataclasses import dataclass, field
from datetime import datetime

from ryax.action_builder.domain.action_build.action_build_exceptions import (
    RepositoryActionResourceRequestInvalidException,
)
from ryax.action_builder.domain.action_build.action_build_values import (
    ActionKind,
    ActionType,
    RepositoryActionIOType,
    RepositoryActionType,
    RepositoryActionKind,
)
from ryax.action_builder.domain.events.base_event import BaseEvent


# Messages from repository
@dataclass
class ActionBuildEvent(BaseEvent):
    action_id: str
    action_technical_name: str
    action_wrapper_type_list: list[str]
    action_type: ActionType
    action_kind: ActionKind
    action_owner_id: str
    action_project: str
    action_version: str
    action_archive: bytes
    action_revision: str


@dataclass
class ActionCancelBuildEvent(BaseEvent):
    action_id: str


# Messages from action builder
@dataclass
class ActionBuildStartEvent(BaseEvent):
    action_id: str


@dataclass
class ActionBuildCanceledEvent(BaseEvent):
    action_id: str


@dataclass
class ActionBuildSuccessEvent(BaseEvent):
    action_id: str


@dataclass
class ActionBuildErrorEvent(BaseEvent):
    action_id: str
    logs: str
    code: int


# messages from studio
@dataclass
class ActionDeletedEvent(BaseEvent):
    """Event received from studio to delete action"""

    action_id: str
    action_version: str


@dataclass
class ActionReadyEvent(BaseEvent):
    """Event to notify repository_action added and ready to use"""

    @dataclass
    class Logo:
        action_logo_id: str
        action_logo_name: str
        action_logo_extension: str
        action_logo_content: bytes | None

    @dataclass
    class ActionResources:
        id: str
        cpu: float | None
        memory: int | None = None
        time: float | None = None
        gpu: int | None = None

        @staticmethod
        def from_metadata(
            cpu: float | None = None,
            memory: Union[int, str] | None = None,
            time: Union[float, str] | None = None,
            gpu: int | None = None,
            minimum_memory_allocation: int = 32 * 1024**2,  # 32MB
        ) -> "ActionReadyEvent.ActionResources":
            times_to_seconds = {"s": 1, "m": 60, "h": 3600, "d": 86400}
            time_units = ",".join(times_to_seconds.keys())
            float_time: float | None = None
            if isinstance(time, float) or isinstance(time, int):
                float_time = float(time)
            elif time is not None and float_time is None:
                try:
                    if time[-1] not in times_to_seconds:
                        raise RepositoryActionResourceRequestInvalidException(
                            f"Time value must end with one of these units: {time_units}"
                        )
                except (TypeError, IndexError):
                    raise RepositoryActionResourceRequestInvalidException(
                        f"Time value must end with one of these units: {time_units}"
                    )
                try:
                    float_time = float(time[:-1]) * times_to_seconds[time[-1]]
                except ValueError:
                    raise RepositoryActionResourceRequestInvalidException(
                        f"Time value must be a float that end with one of these units: {time_units}"
                    )

            if float_time is not None and float_time <= 0:
                raise RepositoryActionResourceRequestInvalidException(
                    "Time value must not be zero or negative"
                )

            memory_in_bytes = {"K": 1024, "M": 1024**2, "G": 1024**3}
            memory_units = ",".join(memory_in_bytes.keys())
            int_memory: int | None = None
            if isinstance(memory, int):
                int_memory = memory
            elif memory is not None and int_memory is None:
                try:
                    if memory[-1] not in memory_units:
                        raise RepositoryActionResourceRequestInvalidException(
                            f"Memory value must end with one of these units: {memory_units}"
                        )
                except (TypeError, IndexError):
                    raise RepositoryActionResourceRequestInvalidException(
                        f"Memory value must end with one of these units: {memory_units}"
                    )
                try:
                    int_memory = int(memory[:-1]) * memory_in_bytes[memory[-1]]
                except ValueError:
                    raise RepositoryActionResourceRequestInvalidException(
                        f"Memory value must be a float that end with one of these units: {memory_units}"
                    )

            if int_memory is not None and int_memory <= minimum_memory_allocation:
                raise RepositoryActionResourceRequestInvalidException(
                    f"Memory value must not be less then {minimum_memory_allocation} bytes to be able to start the action runtime"
                )

            return ActionReadyEvent.ActionResources(
                id=str(uuid.uuid4()),
                cpu=float(cpu) if cpu is not None else None,
                memory=int_memory,
                time=float_time,
                gpu=int(gpu) if gpu is not None else None,
            )

    @dataclass
    class ActionIO:
        """Action io used event"""

        id: str
        technical_name: str
        display_name: str
        type: RepositoryActionIOType
        help: str
        enum_values: list[str] = field(init=True, default_factory=list)
        default_value: str | None = None
        optional: bool = False

    event_type: str = field(init=False, default="ActionReady")
    action_id: str
    action_name: str
    action_technical_name: str
    action_version: str
    action_project_id: str
    action_build_date: datetime
    action_creation_date: datetime
    action_owner_id: str
    action_description: str
    lockfile: bytes
    action_type: RepositoryActionType
    action_kind: RepositoryActionKind
    action_resources: ActionResources | None = None
    action_dynamic_outputs: bool = False
    action_inputs: list[ActionIO] = field(init=True, default_factory=list)
    action_outputs: list[ActionIO] = field(init=True, default_factory=list)
    action_categories: list[str] = field(init=True, default_factory=list)
    action_logo: Logo | None = None
    action_addons: dict[str, dict] = field(init=True, default_factory=dict)


class NoActionBuildEvent(BaseEvent):
    pass
