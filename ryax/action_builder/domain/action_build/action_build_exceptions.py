# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass
from enum import Enum


class RepositoryActionResourceRequestInvalidException(Exception):
    pass


class BuildErrorCode(Enum):
    ImageNameNotDefined = 1
    ActionTypeNotSupported = 2
    InternalRegistryConnection = 3
    ErrorWhileRunningProcess = 4
    RegisteringFunction = 5
    BuildCanceled = 6


@dataclass
class BuildException(Exception):
    code: BuildErrorCode
    logs: str = ""


class RepositoryActionInvalidTypeException(Exception):
    pass


class ActionIOInvalidTypeException(Exception):
    pass


class UnableToOpenLogoFileException(Exception):
    pass


class LogoFileDoesNotExistException(Exception):
    pass


class LogoFileInvalidFileTypeException(Exception):
    pass


class LogoFileTooLargeException(Exception):
    pass


class ActionDynamicOutputsNotPermittedException(Exception):
    pass


class RepositoryActionInvalidKindException(Exception):
    pass
