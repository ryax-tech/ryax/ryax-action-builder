# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from ryax.action_builder.domain.action_build.action_build import ActionBuild
from ryax.action_builder.domain.action_build.action_build_events import ActionBuildEvent
from ryax.action_builder.domain.action_build.action_build_values import (
    ActionKind,
    ActionType,
)


class ActionBuildFactory:
    def from_action_build_event(self, event: ActionBuildEvent) -> ActionBuild:
        return ActionBuild(
            action_id=event.action_id,
            technical_name=event.action_technical_name,
            wrapper_type_list=event.action_wrapper_type_list,
            kind=ActionKind(event.action_kind),
            type=ActionType(event.action_type),
            version=event.action_version,
            archive=event.action_archive,
            owner_id=event.action_owner_id,
            project=event.action_project,
        )
