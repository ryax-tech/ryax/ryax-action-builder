# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import Tuple

import abc

from .action_build import ActionBuild
from .action_build_events import ActionReadyEvent


class IActionBuildProcessor(abc.ABC):
    @abc.abstractmethod
    async def process_build(
        self, action_build: ActionBuild
    ) -> Tuple[bytes, ActionReadyEvent]:
        """Method to process ActionBuild"""
        pass

    @abc.abstractmethod
    async def cleanup(
        self,
        action_build_id: str,
        action_build_version: str,
    ) -> None:
        """Method to remove ActionBuild from registry"""
        pass

    @abc.abstractmethod
    async def cancel_build(self) -> None:
        """Method to cancel a build"""
        pass
