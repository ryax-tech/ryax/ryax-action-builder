# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import Optional

from enum import Enum

from ryax.action_builder.domain.action_build.action_build_exceptions import (
    RepositoryActionInvalidKindException,
    RepositoryActionInvalidTypeException,
)


class ActionType(Enum):
    Python3 = "python3"
    Python3Cuda = "python3-cuda"
    CSharpDotnet6 = "csharp-dotnet6"
    NodeJS = "nodejs"


class ActionWrapperType(Enum):
    PYTHON3_GRPC_V1_TRIGGER = "python3-grpcv1-trigger"
    PYTHON3_GRPC_V1 = "python3-grpcv1"
    PYTHON3_FILE_V1 = "python3-filev1"


class ActionKind(Enum):
    SOURCE = "Source"
    PROCESSOR = "Processor"
    PUBLISHER = "Publisher"


class RepositoryActionKind(Enum):
    PROCESSOR = "Processor"
    SOURCE = "Source"
    PUBLISHER = "Publisher"
    UNDEFINED = "Undefined"

    @classmethod
    def string_to_enum(cls, value: str) -> "RepositoryActionKind":
        for name in cls:
            if name.value == value:
                return name
        else:
            raise RepositoryActionInvalidKindException

    @classmethod
    def has_value(cls, value: str) -> bool:
        return any(value == var.value for var in cls)


class RepositoryActionIOKind(Enum):
    INPUT = "input"
    OUTPUT = "output"


class RepositoryActionType(Enum):
    PYTHON3 = "python3"
    PYTHON3_CUDA = "python3-cuda"
    CSHARP_DOTNET6 = "csharp-dotnet6"
    NODEJS = "nodejs"
    INTERNAL = "internal"
    UNDEFINED = "Undefined"

    @classmethod
    def string_to_enum(cls, value: str) -> "RepositoryActionType":
        for name in cls:
            if name.value == value:
                return name
        else:
            raise RepositoryActionInvalidTypeException


class RepositoryActionIOType(Enum):
    BYTES = "bytes"
    STRING = "string"
    INTEGER = "integer"
    FLOAT = "float"
    BOOLEAN = "boolean"
    FILE = "file"
    DIRECTORY = "directory"
    LONGSTRING = "longstring"
    PASSWORD = "password"
    ENUM = "enum"

    @classmethod
    def has_value(cls, value: Optional[str]) -> bool:
        return any(value == var.value for var in cls)
