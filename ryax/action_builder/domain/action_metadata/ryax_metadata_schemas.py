# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from ryax.action_builder.domain.action_build.action_build_values import (
    RepositoryActionKind,
    RepositoryActionIOType,
    RepositoryActionType,
)


class RyaxMetadataSchemaV2:
    schema: dict = {
        "type": "object",
        "additionalProperties": False,
        "properties": {
            "apiVersion": {"type": "string", "const": "ryax.tech/v2.0"},
            "kind": {
                "type": "string",
                "enum": [item.value for item in RepositoryActionKind],
            },
            "spec": {
                "additionalProperties": False,
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "minLength": 3,
                        "maxLength": 24,
                    },
                    "human_name": {
                        "type": "string",
                        "minLength": 3,
                        "maxLength": 128,
                    },
                    "type": {
                        "type": "string",
                        "enum": [item.value for item in RepositoryActionType],
                    },
                    "dependencies": {
                        "type": "array",
                        "maxItems": 64,
                        "items": {
                            "type": "string",
                            "maxLength": 1024,
                        },
                    },
                    "version": {"type": "string", "minLength": 3, "maxLength": 128},
                    "logo": {"type": "string"},
                    "addons": {
                        "type": "object",
                        "description": "The addons as key-values, with the values being an object containing parametrisation of the addon",
                    },
                    "options": {
                        "type": "object",
                        "description": "Key values to give options to Ryax",
                    },
                    "inputs": {
                        "type": "array",
                        "description": "The list of action's inputs.",
                        "items": {
                            "type": "object",
                            "additionalProperties": False,
                            "properties": {
                                "human_name": {
                                    "type": "string",
                                    "minLength": 0,
                                    "maxLength": 128,
                                },
                                "help": {
                                    "type": "string",
                                    "minLength": 0,
                                    "maxLength": 10_000,
                                },
                                "name": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 64,
                                },
                                "type": {
                                    "type": "string",
                                    "enum": [
                                        item.value for item in RepositoryActionIOType
                                    ],
                                },
                                "enum_values": {
                                    "type": "array",
                                    "items": {
                                        "type": "string",
                                        "minLength": 1,
                                        "maxLength": 10_000,
                                    },
                                    "maxItems": 64,
                                },
                                "default_value": {
                                    "type": ["string", "number", "boolean"],
                                },
                                "optional": {
                                    "type": ["boolean"],
                                },
                            },
                            "required": [
                                "human_name",
                                "help",
                                "name",
                                "type",
                            ],
                        },
                    },
                    "outputs": {
                        "type": "array",
                        "description": "The list of action's outputs.",
                        "maxItems": 64,
                        "items": {
                            "type": "object",
                            "additionalProperties": False,
                            "properties": {
                                "human_name": {
                                    "type": "string",
                                    "minLength": 0,
                                    "maxLength": 128,
                                },
                                "help": {
                                    "type": "string",
                                    "minLength": 0,
                                    "maxLength": 10_000,
                                },
                                "name": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 64,
                                },
                                "type": {
                                    "type": "string",
                                    "enum": [
                                        item.value for item in RepositoryActionIOType
                                    ],
                                },
                                "enum_values": {
                                    "type": "array",
                                    "items": {
                                        "type": "string",
                                        "minLength": 1,
                                        "maxLength": 10_000,
                                    },
                                    "maxItems": 64,
                                },
                                "optional": {
                                    "type": ["boolean"],
                                },
                            },
                            "required": [
                                "human_name",
                                "help",
                                "name",
                                "type",
                            ],
                        },
                    },
                    "dynamic_outputs": {
                        "description": "If True, this action has dynamic outputs. Which means that the outputs of this action will be defined independently for each instance.",
                        "type": "boolean",
                    },
                    "description": {"type": "string"},
                    "categories": {
                        "type": "array",
                        "description": "Categories of the actions.",
                        "maxItems": 32,
                        "items": {
                            "type": "string",
                            "minLength": 1,
                            "maxLength": 32,
                        },
                    },
                    "resources": {
                        "additionalProperties": False,
                        "type": "object",
                        "properties": {
                            "cpu": {"type": "number", "minimum": 0.1},
                            "memory": {
                                "anyOf": [
                                    {"type": "string", "minLength": 2, "maxLength": 24},
                                    {"type": "number", "minimum": 1_000_000},
                                ]
                            },
                            "time": {
                                "anyOf": [
                                    {"type": "string", "minLength": 2, "maxLength": 32},
                                    {"type": "number", "minimum": 1},
                                ]
                            },
                            "gpu": {"type": "number", "minimum": 1},
                        },
                    },
                },
                "required": ["id", "human_name", "type"],
            },
        },
        "required": ["apiVersion", "kind", "spec"],
    }
