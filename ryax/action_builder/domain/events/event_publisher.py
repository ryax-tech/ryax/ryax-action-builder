# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from typing import List

from .base_event import BaseEvent


class IEventPublisher(abc.ABC):
    @abc.abstractmethod
    async def publish(self, events: List[BaseEvent]) -> None:
        """Method used to publish domain event"""
        pass
