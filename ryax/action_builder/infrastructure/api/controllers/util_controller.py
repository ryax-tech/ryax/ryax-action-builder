# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from aiohttp import web
from aiohttp.web_response import Response
from aiohttp_apispec import docs
from dependency_injector.wiring import Provide

from ryax.action_builder.application.util_service import UtilService
from ryax.action_builder.container import ApplicationContainer


@docs(
    tags=["Monitoring"],
    summary="Check service status",
    description="Help to know service status",
    responses={
        200: {"description": "Service healthy"},
        400: {"description": "Service unhealthy"},
    },
    security=[],
)
async def health_check(
    request: web.Request,
    service: UtilService = Provide[ApplicationContainer.util_service],
) -> Response:
    if service.is_healthy():
        return web.json_response("Service is healthy", status=200)
    else:
        return web.json_response("Service is unhealthy", status=400)
