# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from aiohttp import web

from ryax.action_builder.container import ApplicationContainer
from ryax.action_builder.infrastructure.api.controllers import util_controller


def setup(app: web.Application, container: ApplicationContainer) -> None:
    """Method to setup api"""
    # Configure application container for wiring
    container.wire(
        modules=[
            util_controller,
        ]
    )

    # Configure api routing
    app.add_routes(
        [
            web.get("/healthz", util_controller.health_check, allow_head=False),
        ]
    )
