# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import contextlib

import uuid

from PIL import Image
from datetime import datetime

import asyncio.subprocess
import logging
import shutil
import tempfile
import yaml
import asyncio
from pathlib import Path
from typing import Any, Tuple, Generator
from tempfile import NamedTemporaryFile

from ryax.action_builder.domain.action_build.action_build import (
    ActionBuild,
)
from ryax.action_builder.domain.action_build.action_build_events import ActionReadyEvent
from ryax.action_builder.domain.action_build.action_build_exceptions import (
    BuildErrorCode,
    BuildException,
    ActionDynamicOutputsNotPermittedException,
    ActionIOInvalidTypeException,
    UnableToOpenLogoFileException,
    LogoFileDoesNotExistException,
    LogoFileInvalidFileTypeException,
    LogoFileTooLargeException,
)
from ryax.action_builder.domain.action_build.action_build_processor import (
    IActionBuildProcessor,
)
from ryax.action_builder.domain.action_build.action_build_values import (
    ActionType,
    ActionWrapperType,
    RepositoryActionIOType,
    RepositoryActionKind,
    RepositoryActionType,
)
from ryax.action_builder.infrastructure.builder.external_commands import (
    ExternalCommandsService,
)

logger = logging.getLogger(__name__)


class BuilderService(IActionBuildProcessor):
    def __init__(
        self, external_commands_service: ExternalCommandsService, internal_registry: str
    ):
        self.internal_registry = internal_registry
        self.external_commands_service: ExternalCommandsService = (
            external_commands_service
        )

    async def process_build(
        self, action_build: ActionBuild
    ) -> Tuple[bytes, ActionReadyEvent]:
        with NamedTemporaryFile(suffix=".zip") as archive_file:
            archive_file.write(action_build.archive)
            archive_file.flush()
            archive_path: Path = Path(archive_file.name)
            if action_build.type in ActionType:
                with self.unpack(archive_path) as tmp_dir:
                    metadata = await self.build(action_build, tmp_dir)

                    # Path to the lockfile that will be created at the end
                    # of the ryax-build script.
                    lockfile_path = tmp_dir + "/ryax_lock.json"

                    # Gather information that should have been locked
                    # during the build process
                    lockfile = await self._gather_lock_informations(lockfile_path)

                    # Right after the action is built we use the metadata
                    # and the unpacked archive to load the information
                    # for studio
                    action_ready_event = self.build_action_ready_event(
                        tmp_dir, action_build, metadata, lockfile
                    )

                    return lockfile, action_ready_event
            else:
                raise BuildException(code=BuildErrorCode.ActionTypeNotSupported)

    def build_action_ready_event(
        self,
        archive_path: str,
        action_build: ActionBuild,
        metadata: dict,
        lockfile: bytes,
    ) -> ActionReadyEvent:
        action_inputs, action_outputs = [], []

        for metadata_input in metadata["spec"].get("inputs", []):
            if not RepositoryActionIOType.has_value(metadata_input["type"]):
                raise ActionIOInvalidTypeException
            action_input_default_value = metadata_input.get("default_value", None)
            action_input = ActionReadyEvent.ActionIO(
                id=str(uuid.uuid4()),
                display_name=metadata_input["human_name"],
                technical_name=metadata_input["name"],
                help=metadata_input["help"],
                type=RepositoryActionIOType(metadata_input["type"]),
                # kind=RepositoryActionIOKind.INPUT,
                enum_values=metadata_input.get("enum_values", []),
                default_value=str(action_input_default_value)
                if action_input_default_value is not None
                else None,
                optional=metadata_input.get("optional", False),
            )
            action_inputs.append(action_input)

        for metadata_output in metadata["spec"].get("outputs", []):
            if not RepositoryActionIOType.has_value(metadata_output["type"]):
                raise ActionIOInvalidTypeException

            action_output = ActionReadyEvent.ActionIO(
                id=str(uuid.uuid4()),
                display_name=metadata_output["human_name"],
                technical_name=metadata_output["name"],
                help=metadata_output["help"],
                type=RepositoryActionIOType(metadata_output["type"]),
                # kind=RepositoryActionIOKind.OUTPUT,
                enum_values=metadata_output.get("enum_values", []),
                optional=metadata_output.get("optional", False),
            )
            action_outputs.append(action_output)

        ar_event = ActionReadyEvent(
            action_project_id=action_build.project,
            action_owner_id=action_build.owner_id,
            action_logo=None,
            action_version=metadata["spec"].get("version", ""),
            action_id=action_build.action_id,
            action_build_date=datetime.now(),
            action_resources=ActionReadyEvent.ActionResources.from_metadata(
                cpu=metadata["spec"]["resources"].get("cpu"),
                memory=metadata["spec"]["resources"].get("memory"),
                time=metadata["spec"]["resources"].get("time"),
                gpu=metadata["spec"]["resources"].get("gpu"),
            )
            if metadata["spec"].get("resources") is not None
            else None,
            action_inputs=action_inputs,
            action_kind=RepositoryActionKind.string_to_enum(metadata["kind"]),
            action_type=RepositoryActionType.string_to_enum(metadata["spec"]["type"]),
            action_name=metadata["spec"]["human_name"],
            action_categories=list(set(metadata["spec"].get("categories", []))),
            action_description=metadata["spec"]["description"],
            action_addons=metadata["spec"].get("addons", {}),
            action_outputs=action_outputs,
            action_technical_name=metadata["spec"]["id"],
            action_creation_date=datetime.now(),
            action_dynamic_outputs=metadata["spec"].get("dynamic_outputs", False),
            lockfile=lockfile,
        )

        ar_event.action_dynamic_outputs = metadata["spec"].get("dynamic_outputs", False)
        if (
            ar_event.action_kind != RepositoryActionKind.SOURCE
            and ar_event.action_dynamic_outputs is True
        ):
            raise ActionDynamicOutputsNotPermittedException()

        metadata_logo: str = metadata["spec"].get("logo", None)
        if metadata_logo is not None:
            logo_path = Path(metadata_logo)

            logo_complete_path = Path(archive_path) / logo_path

            BuilderService.check_logo_file(logo_complete_path)
            logo_content = self.load_logo_file(logo_complete_path)
            action_logo = ActionReadyEvent.Logo(
                action_logo_name=logo_path.stem,
                action_logo_extension=logo_path.suffix,
                action_logo_content=logo_content,
                action_logo_id=str(uuid.uuid4()),
            )
            ar_event.action_logo = action_logo

        return ar_event

    def load_logo_file(self, logo_path: Path) -> bytes:
        """Loads the repository_action logo"""
        try:
            logo_content = self._load_file_content_as_bytes(str(logo_path))
            return logo_content
        except Exception:
            raise UnableToOpenLogoFileException

    @staticmethod
    def _load_file_content_as_bytes(file_path: str) -> bytes:
        """Reads a file in binary mode and returns the content"""
        with open(file_path, "rb") as file:
            content = file.read()
        return content

    @staticmethod
    def check_logo_file(logo: Path) -> None:
        if not logo.is_file():
            raise LogoFileDoesNotExistException
        elif logo.suffix not in [".png", ".jpg", ".jpeg"]:
            raise LogoFileInvalidFileTypeException
        else:
            with Image.open(logo) as img:
                width, height = img.size
            if width > 250 or height > 250:
                raise LogoFileTooLargeException

    def _get_container_image_name(
        self,
        action_build_id: str,
        action_build_version: str,
        action_wrapper_type: ActionWrapperType,
    ) -> str:
        if action_wrapper_type == ActionWrapperType.PYTHON3_FILE_V1:
            return f"{self.internal_registry}/{action_build_id}-{str(action_wrapper_type.value).lower()}:{action_build_version}"
        else:
            return f"{self.internal_registry}/{action_build_id}:{action_build_version}"

    #  add a name pattern on the version of the action
    def _get_build_and_push_command(
        self,
        action_build: ActionBuild,
        tmp_dir: str,
        action_wrapper_type: ActionWrapperType,
    ) -> str:
        # Use offload by default, unless it is a trigger
        return self.external_commands_service.build_action_image(
            action_build.action_id,
            str(action_wrapper_type.value),
            str(action_build.type.value),
            tmp_dir,
            action_build.version,
            self.internal_registry,
        )

    async def cancel_build(self) -> None:
        logger.info("Canceling build")
        await self.external_commands_service.cancel()

    @staticmethod
    async def _load_file_content(path: str, dont_strip: bool = False) -> str | None:
        cmd = f"cat {path}"
        # Run the command asynchronously
        proc = await asyncio.create_subprocess_shell(
            cmd, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE
        )

        stdout, _ = await proc.communicate()

        if proc.returncode == 0:
            result = stdout.decode()
            if dont_strip:
                return result
            return result.strip()
        return None

    async def _gather_lock_informations(self, lockfile_path: str) -> bytes:
        """
        After calling the `nix-build` script from wrappers, one should find the
        locked information used to build the action.
        """
        content = await self._load_file_content(lockfile_path)

        return bytes(content or "", "utf-8")

    @contextlib.contextmanager
    def unpack(self, archive_path: Path) -> Generator[str, None, None]:
        with tempfile.TemporaryDirectory() as tmp_dir:
            shutil.unpack_archive(archive_path, tmp_dir)
            yield tmp_dir

    async def build(
        self,
        action_build: ActionBuild,
        tmp_dir: str,
    ) -> dict[str, Any]:
        metadata = yaml.safe_load(
            await self._load_file_content(tmp_dir + "/ryax_metadata.yaml") or ""
        )

        for wrapper_type in action_build.wrapper_type_list:
            build_command = self._get_build_and_push_command(
                action_build, tmp_dir, ActionWrapperType(wrapper_type)
            )
            await self.external_commands_service.execute_in_shell(
                f"{action_build.kind.value} build", build_command
            )
        return metadata

    async def cleanup(
        self,
        action_build_id: str,
        action_build_version: str,
    ) -> None:
        images_to_delete = set()
        for action_wrapper_type in ActionWrapperType:
            image_name = self._get_container_image_name(
                action_build_id, action_build_version, action_wrapper_type
            )
            images_to_delete.add(image_name)
        for image_name in images_to_delete:
            try:
                await self.external_commands_service.delete_image(image_name)
            except (Exception, BuildException):
                logger.info(
                    "Unable to delete the container image %s, might be due to wrapper never built. skipping this step",
                    image_name,
                )

    async def garbage_collect(self, availability_ratio_threshold: float) -> None:
        """Run a garbage collection if the availability threshold is reached.
        The threshold is a number between 0 and 1 that represents the ratio of available storage.
        For example: 0.8 means that the GC will be triggered if the available disk is more then 80% full.
        """
        free_size, total_size = self.external_commands_service.get_nix_store_stats()

        if (1 - (free_size / total_size)) > availability_ratio_threshold:
            logger.info(
                f"Nix store availability: Free {free_size:.1f}G / Total {total_size:.1f}G. Nix store is almost full, disk usage is above {int(availability_ratio_threshold * 100)}%. Running garbage collection"
            )
            await self.external_commands_service.run_nix_garbage_collection()
        else:
            logger.info(
                f"Nix store availability: Free {free_size:.1f}G / Total {total_size:.1f}G. Do not trigger GC"
            )
