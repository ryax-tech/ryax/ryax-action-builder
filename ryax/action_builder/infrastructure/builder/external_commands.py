# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import asyncio
import os
from io import TextIOWrapper
from logging import Logger, getLogger
from asyncio.subprocess import Process
from tempfile import TemporaryFile
from typing import Tuple, Optional

from ryax.action_builder.domain.action_build.action_build_exceptions import (
    BuildErrorCode,
    BuildException,
)

logger: Logger = getLogger(__name__)


class ExternalCommandsService:
    def __init__(self, wrappers_dir: str, extra_nix_args: str):
        self.builder_dir = wrappers_dir
        self.extra_nix_args = extra_nix_args
        self.current_build: Optional[Process] = None

    async def start_subprocess(
        self, cmd: str, env: dict[str, str] = {}
    ) -> Tuple[Process, TextIOWrapper, TextIOWrapper]:
        stdout = stderr = None
        try:
            stdout = TemporaryFile(mode="w+", encoding="utf-8")
            stderr = TemporaryFile(mode="w+", encoding="utf-8")

            proc = await asyncio.create_subprocess_shell(
                cmd, stdout=stdout, stderr=stderr, env=env
            )
            return proc, stdout, stderr

        # Not sure if its necessary but I don't want to have remaining opened file that
        # could crash the container
        except Exception as e:
            logger.warning(
                "Starting command has raised an error, closing stdout and stderr"
            )
            if stdout:
                stdout.close()
            if stderr:
                stderr.close()
            raise e

    async def wait_end_of_process(
        self, proc: Process, cmd_name: str, stdout: TextIOWrapper, stderr: TextIOWrapper
    ) -> str:
        # wait for completion
        out = ""
        err = ""
        try:
            await proc.communicate()
            if not stdout.closed:
                stdout.seek(0)
                out = stdout.read()
            if not stderr.closed:
                stderr.seek(0)
                err = stderr.read()
        finally:
            if stdout:
                stdout.close()
            if stderr:
                stderr.close()

        if proc.returncode == 0:
            status = "Done: "
            logger.debug(
                f"{status}: {cmd_name} (pid = {str(proc.pid)})" f"\nSTDOUT: {out}"
            )
            return out
        else:
            status = "Failed: "
            logger.debug(
                f"{status}: {cmd_name} (pid = {str(proc.pid)})"
                f"\nSTDOUT: {out}"
                f"\nSTDERR: {err}"
            )
            logger.info(
                f"Error: while running process {cmd_name} (error_code={proc.returncode}): \nout: {out}\nerr: {err}"
            )
            raise BuildException(
                code=(
                    BuildErrorCode.BuildCanceled
                    if proc.returncode == -15
                    else BuildErrorCode.ErrorWhileRunningProcess
                ),
                logs=f"Error: while running process {cmd_name}: {out}\n{err}",
            )

    async def execute_in_shell(
        self, cmd_name: str, cmd: str, env: dict[str, str] = {}
    ) -> str:
        logger.info(f">>> {cmd}")

        # Create process env with current environment and env passed in parameter
        process_env = os.environ.copy() | env

        (proc, stdout, stderr) = await self.start_subprocess(cmd, env=process_env)

        # Keep track of current build to be able to cancel
        self.current_build = proc

        result = await self.wait_end_of_process(proc, cmd_name, stdout, stderr)

        # Clean process
        self.current_build = None

        return result

    async def cancel(self) -> None:
        if self.current_build:
            logger.info("Cancel current build")
            try:
                self.current_build.terminate()
            except ProcessLookupError:
                logger.warning(
                    "Unable to find build process during cancel, ignoring..."
                )
        else:
            logger.warning("Ask to cancel but no current build")

    def build_action_image(
        self,
        action_name: str,
        wrapper_type: str,
        action_type: str,
        action_dir: str,
        version: str,
        registry: str,
    ) -> str:
        return f"EXTRA_NIX_ARGS={self.extra_nix_args} {self.builder_dir}/ryax-build {action_dir} {action_name} {version} {action_type} {wrapper_type} {registry}"

    async def delete_image(self, img_name: str) -> None:
        await self.execute_in_shell(
            f"Deleting image {img_name} from the registry",
            f"skopeo delete --insecure-policy --tls-verify=false docker://{img_name}",
        )
        logger.info(f"Image deleted: {img_name}")

    async def run_nix_garbage_collection(self) -> None:
        await asyncio.subprocess.create_subprocess_shell("nix-store --gc")

    def get_nix_store_stats(self) -> tuple[float, float]:
        stat = os.statvfs("/nix")
        free_size = stat.f_bavail * stat.f_frsize / 1024**3
        total_size = stat.f_blocks * stat.f_frsize / 1024**3
        return free_size, total_size
