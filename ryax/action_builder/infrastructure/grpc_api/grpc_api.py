# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass
from logging import getLogger
from typing import cast

from ryax.action_builder.application.action_build_locksync import (
    ActionBuildLockSyncService,
)
from ryax.action_builder.application.action_build_service import ActionBuildService
from ryax.action_builder.domain.action_build.action_build_events import (
    ActionBuildStartEvent,
    ActionBuildSuccessEvent,
    ActionBuildErrorEvent,
    ActionBuildCanceledEvent,
    ActionBuildEvent,
    ActionCancelBuildEvent,
)

from collections.abc import AsyncIterator

import grpc
from opentelemetry.instrumentation.grpc import GrpcAioInstrumentorClient

from ryax.action_builder.infrastructure.grpc_api.utils import (
    convert_build_request_to_event,
)
from ryax.action_builder.infrastructure.messaging.messages.action_builder_messages_pb2 import (
    ActionBuildCanceled,
)
from ryax.action_builder.infrastructure.messaging.messages.grpc_messages_pb2 import (
    BuildReply,
    BuildRequest,
)
from ryax.action_builder.infrastructure.messaging.messages.grpc_messages_pb2_grpc import (
    ActionBuilderServicer,
    add_ActionBuilderServicer_to_server,
)
from ryax.action_builder.infrastructure.messaging.messages.repository_messages_pb2 import (
    ActionCancelBuild,
)

logger = getLogger(__name__)

GrpcAioInstrumentorClient().instrument()


class ActionBuilderGRPCHandler(ActionBuilderServicer):
    def __init__(
        self,
        action_build_service: ActionBuildService,
        lock_build_service: ActionBuildLockSyncService,
    ) -> None:
        self.service = action_build_service
        self.lock_build_service = lock_build_service

    async def _handle_build_request(
        self, request_event: ActionBuildEvent
    ) -> AsyncIterator[BuildReply]:
        if not self.lock_build_service.lock_build():
            yield BuildReply(
                status=BuildReply.Status.BUILD_ERROR,
                logs="A build is already occurring",
            )
            return

        try:
            final_reply = BuildReply()
            async for code, value in self.service.build(request_event):
                if code == 0:
                    build_event = value
                    if isinstance(build_event, ActionBuildStartEvent):
                        reply = BuildReply()
                        reply.status = BuildReply.Status.BUILD_STARTED
                        yield reply
                    elif isinstance(build_event, ActionBuildSuccessEvent):
                        final_reply.status = BuildReply.Status.BUILD_SUCCESS
                        final_reply.logs = f"Build successful for action {request_event.action_technical_name} ({request_event.action_id})"
                        continue
                    elif isinstance(build_event, ActionBuildErrorEvent):
                        final_reply.status = BuildReply.Status.BUILD_ERROR
                        continue
                    elif isinstance(build_event, ActionBuildCanceledEvent):
                        final_reply.status = BuildReply.Status.BUILD_CANCELLED
                        continue
                    else:
                        raise Exception(f"Unknown event {request_event}")

                if code == 1:
                    final_reply.lockfile = cast(bytes, value)

            yield final_reply

        except Exception as err:
            logger.error("Build failed", exc_info=err, stack_info=True)
            yield BuildReply(status=BuildReply.Status.BUILD_ERROR, logs=str(err))

        finally:
            self.lock_build_service.unlock_build()

    async def CancelBuild(
        self,
        request: ActionCancelBuild,
        context: grpc.aio.ServicerContext,
    ) -> ActionBuildCanceled:
        logger.info("Received: %s", request)
        event = ActionCancelBuildEvent(action_id=request.action_id)
        await self.service.cancel_build(event)

        return ActionBuildCanceled(action_id=request.action_id)

    async def Build(
        self, request: BuildRequest, context: grpc.aio.ServicerContext
    ) -> AsyncIterator[BuildReply]:
        event = convert_build_request_to_event(request)

        async for stream_reply in self._handle_build_request(event):
            yield stream_reply


@dataclass
class GRPCAPIService:
    service: ActionBuildService
    action_build_locksync: ActionBuildLockSyncService
    server_port: str

    async def serve(
        self,
    ) -> None:
        server = grpc.aio.server()
        add_ActionBuilderServicer_to_server(
            ActionBuilderGRPCHandler(
                action_build_service=self.service,
                lock_build_service=self.action_build_locksync,
            ),
            server,
        )

        server_ip = f"0.0.0.0:{self.server_port}"
        server.add_insecure_port(server_ip)
        logger.debug(f"Server is running on {server_ip}")

        await server.start()
        await server.wait_for_termination()
        await server.stop(5)
        logger.debug("Server stopped")
