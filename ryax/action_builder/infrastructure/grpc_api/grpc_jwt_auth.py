from typing import Callable, Awaitable, Any

import grpc

_JWT_HEADER_KEY = "x-signature"


class SignatureValidationInterceptor(grpc.aio.ServerInterceptor):
    def __init__(self) -> None:
        def abort(ignored_request: Any, context: grpc.aio.ServicerContext) -> None:
            _ = context.abort(grpc.StatusCode.UNAUTHENTICATED, "Invalid signature")

        self._abort_handler = grpc.unary_stream_rpc_method_handler(abort)

    async def intercept_service(
        self,
        continuation: Callable[
            [grpc.HandlerCallDetails], Awaitable[grpc.RpcMethodHandler]
        ],
        handler_call_details: grpc.HandlerCallDetails,
    ) -> grpc.RpcMethodHandler:
        # Example HandlerCallDetails object:
        #     _HandlerCallDetails(
        #       method=u'/helloworld.Greeter/SayHello',
        #       invocation_metadata=...)
        method_name = handler_call_details.method.split("/")[-1]
        expected_metadata = (_JWT_HEADER_KEY, method_name[::-1])
        if expected_metadata in handler_call_details.invocation_metadata:
            return await continuation(handler_call_details)
        else:
            return self._abort_handler
