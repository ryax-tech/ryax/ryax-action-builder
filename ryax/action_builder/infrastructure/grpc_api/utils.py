# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

from ryax.action_builder.domain.action_build.action_build_events import (
    ActionBuildEvent,
)
from ryax.action_builder.domain.action_build.action_build_values import (
    ActionType,
    ActionKind,
)


from ryax.action_builder.infrastructure.messaging.messages.grpc_messages_pb2 import (
    BuildRequest,
)


def convert_build_request_to_event(request: BuildRequest) -> ActionBuildEvent:
    wrapper_list = []

    for i in request.action_wrapper_type_list:
        wrapper_list.append(i)

    event = ActionBuildEvent(
        action_id=request.action_id,
        action_type=ActionType(request.action_type),
        action_kind=ActionKind(request.action_kind),
        action_version=request.action_version,
        action_archive=request.action_archive,
        action_wrapper_type_list=wrapper_list,
        action_technical_name=request.action_technical_name,
        action_revision=request.action_revision,
        action_project=request.action_project,
        action_owner_id=request.action_owner_id,
    )

    return event
