# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from aio_pika.abc import AbstractIncomingMessage
from dependency_injector.wiring import Provide

from ryax.action_builder.application.action_build_service import ActionBuildService
from ryax.action_builder.container import ApplicationContainer
from ryax.action_builder.domain.action_build.action_build_events import (
    ActionBuildEvent,
    ActionDeletedEvent,
    ActionCancelBuildEvent,
)
from ryax.action_builder.domain.action_build.action_build_values import (
    ActionKind,
    ActionType,
)
from ryax.action_builder.infrastructure.messaging.messages.repository_messages_pb2 import (
    ActionBuild,
    ActionCancelBuild,
)
from ryax.action_builder.infrastructure.messaging.messages.studio_messages_pb2 import (
    ActionDeleted,
)


async def on_action_build(
    message: AbstractIncomingMessage,
    service: ActionBuildService = Provide[
        ApplicationContainer.action_build_service_broker
    ],
) -> None:
    message_content = ActionBuild()
    message_content.ParseFromString(message.body)
    # Convert RepeatedScalarFieldContainer[str] to list
    wrapper_list = []
    for i in message_content.action_wrapper_type_list:
        wrapper_list.append(i)
    event = ActionBuildEvent(
        action_id=message_content.action_id,
        action_type=ActionType(message_content.action_type),
        action_kind=ActionKind(message_content.action_kind),
        action_version=message_content.action_version,
        action_archive=message_content.action_archive,
        action_wrapper_type_list=wrapper_list,
        action_technical_name=message_content.action_technical_name,
        action_revision=message_content.action_revision,
        action_owner_id=message_content.owner_id,
        action_project=message_content.project,
    )

    await service.process_action_build(event)


async def on_action_build_cancel(
    message: AbstractIncomingMessage,
    service: ActionBuildService = Provide[
        ApplicationContainer.action_build_service_broker
    ],
) -> None:
    message_content = ActionCancelBuild()
    message_content.ParseFromString(message.body)

    event = ActionCancelBuildEvent(action_id=message_content.action_id)
    await service.cancel_build(event)


async def on_action_deleted(
    message: AbstractIncomingMessage,
    service: ActionBuildService = Provide[
        ApplicationContainer.action_build_service_broker
    ],
) -> None:
    message_content = ActionDeleted()
    message_content.ParseFromString(message.body)
    event = ActionDeletedEvent(
        action_id=message_content.action_id,
        action_version=message_content.action_version,
    )
    await service.delete_action(event)
