"""
@generated by mypy-protobuf.  Do not edit manually!
isort:skip_file
"""

import builtins
import collections.abc
import google.protobuf.descriptor
import google.protobuf.internal.containers
import google.protobuf.internal.enum_type_wrapper
import google.protobuf.message
import sys
import typing

if sys.version_info >= (3, 10):
    import typing as typing_extensions
else:
    import typing_extensions

DESCRIPTOR: google.protobuf.descriptor.FileDescriptor

@typing.final
class CancelCurrentBuildReply(google.protobuf.message.Message):
    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    ACTION_ID_FIELD_NUMBER: builtins.int
    action_id: builtins.str
    def __init__(
        self,
        *,
        action_id: builtins.str = ...,
    ) -> None: ...
    def ClearField(self, field_name: typing.Literal["action_id", b"action_id"]) -> None: ...

global___CancelCurrentBuildReply = CancelCurrentBuildReply

@typing.final
class BuildRequest(google.protobuf.message.Message):
    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    ACTION_ID_FIELD_NUMBER: builtins.int
    ACTION_TYPE_FIELD_NUMBER: builtins.int
    ACTION_KIND_FIELD_NUMBER: builtins.int
    ACTION_VERSION_FIELD_NUMBER: builtins.int
    ACTION_ARCHIVE_FIELD_NUMBER: builtins.int
    ACTION_TECHNICAL_NAME_FIELD_NUMBER: builtins.int
    ACTION_REVISION_FIELD_NUMBER: builtins.int
    ACTION_WRAPPER_TYPE_LIST_FIELD_NUMBER: builtins.int
    ACTION_OWNER_ID_FIELD_NUMBER: builtins.int
    ACTION_PROJECT_FIELD_NUMBER: builtins.int
    action_id: builtins.str
    action_type: builtins.str
    action_kind: builtins.str
    action_version: builtins.str
    action_archive: builtins.bytes
    action_technical_name: builtins.str
    action_revision: builtins.str
    action_owner_id: builtins.str
    action_project: builtins.str
    @property
    def action_wrapper_type_list(self) -> google.protobuf.internal.containers.RepeatedScalarFieldContainer[builtins.str]: ...
    def __init__(
        self,
        *,
        action_id: builtins.str = ...,
        action_type: builtins.str = ...,
        action_kind: builtins.str = ...,
        action_version: builtins.str = ...,
        action_archive: builtins.bytes = ...,
        action_technical_name: builtins.str = ...,
        action_revision: builtins.str = ...,
        action_wrapper_type_list: collections.abc.Iterable[builtins.str] | None = ...,
        action_owner_id: builtins.str = ...,
        action_project: builtins.str = ...,
    ) -> None: ...
    def ClearField(self, field_name: typing.Literal["action_archive", b"action_archive", "action_id", b"action_id", "action_kind", b"action_kind", "action_owner_id", b"action_owner_id", "action_project", b"action_project", "action_revision", b"action_revision", "action_technical_name", b"action_technical_name", "action_type", b"action_type", "action_version", b"action_version", "action_wrapper_type_list", b"action_wrapper_type_list"]) -> None: ...

global___BuildRequest = BuildRequest

@typing.final
class BuildReply(google.protobuf.message.Message):
    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    class _Status:
        ValueType = typing.NewType("ValueType", builtins.int)
        V: typing_extensions.TypeAlias = ValueType

    class _StatusEnumTypeWrapper(google.protobuf.internal.enum_type_wrapper._EnumTypeWrapper[BuildReply._Status.ValueType], builtins.type):
        DESCRIPTOR: google.protobuf.descriptor.EnumDescriptor
        NONE: BuildReply._Status.ValueType  # 0
        BUILD_SUCCESS: BuildReply._Status.ValueType  # 1
        BUILD_STARTED: BuildReply._Status.ValueType  # 2
        BUILD_CANCELLED: BuildReply._Status.ValueType  # 3
        BUILD_ERROR: BuildReply._Status.ValueType  # 4

    class Status(_Status, metaclass=_StatusEnumTypeWrapper): ...
    NONE: BuildReply.Status.ValueType  # 0
    BUILD_SUCCESS: BuildReply.Status.ValueType  # 1
    BUILD_STARTED: BuildReply.Status.ValueType  # 2
    BUILD_CANCELLED: BuildReply.Status.ValueType  # 3
    BUILD_ERROR: BuildReply.Status.ValueType  # 4

    STATUS_FIELD_NUMBER: builtins.int
    LOGS_FIELD_NUMBER: builtins.int
    LOCKFILE_FIELD_NUMBER: builtins.int
    status: global___BuildReply.Status.ValueType
    logs: builtins.str
    lockfile: builtins.bytes
    def __init__(
        self,
        *,
        status: global___BuildReply.Status.ValueType = ...,
        logs: builtins.str = ...,
        lockfile: builtins.bytes = ...,
    ) -> None: ...
    def ClearField(self, field_name: typing.Literal["lockfile", b"lockfile", "logs", b"logs", "status", b"status"]) -> None: ...

global___BuildReply = BuildReply
