# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

from ryax.action_builder.infrastructure.messaging.messages import (
    action_builder_messages_pb2 as ryax_dot_action__builder_dot_infrastructure_dot_messaging_dot_messages_dot_action__builder__messages__pb2,
)
from ryax.action_builder.infrastructure.messaging.messages import (
    grpc_messages_pb2 as ryax_dot_action__builder_dot_infrastructure_dot_messaging_dot_messages_dot_grpc__messages__pb2,
)
from ryax.action_builder.infrastructure.messaging.messages import (
    repository_messages_pb2 as ryax_dot_action__builder_dot_infrastructure_dot_messaging_dot_messages_dot_repository__messages__pb2,
)


class ActionBuilderStub(object):
    """Manage Ryax Action executions"""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.Build = channel.unary_stream(
            "/grpc_api.ActionBuilder/Build",
            request_serializer=ryax_dot_action__builder_dot_infrastructure_dot_messaging_dot_messages_dot_grpc__messages__pb2.BuildRequest.SerializeToString,
            response_deserializer=ryax_dot_action__builder_dot_infrastructure_dot_messaging_dot_messages_dot_grpc__messages__pb2.BuildReply.FromString,
        )
        self.CancelBuild = channel.unary_unary(
            "/grpc_api.ActionBuilder/CancelBuild",
            request_serializer=ryax_dot_action__builder_dot_infrastructure_dot_messaging_dot_messages_dot_repository__messages__pb2.ActionCancelBuild.SerializeToString,
            response_deserializer=ryax_dot_action__builder_dot_infrastructure_dot_messaging_dot_messages_dot_action__builder__messages__pb2.ActionBuildCanceled.FromString,
        )


class ActionBuilderServicer(object):
    """Manage Ryax Action executions"""

    def Build(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details("Method not implemented!")
        raise NotImplementedError("Method not implemented!")

    def CancelBuild(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details("Method not implemented!")
        raise NotImplementedError("Method not implemented!")


def add_ActionBuilderServicer_to_server(servicer, server):
    rpc_method_handlers = {
        "Build": grpc.unary_stream_rpc_method_handler(
            servicer.Build,
            request_deserializer=ryax_dot_action__builder_dot_infrastructure_dot_messaging_dot_messages_dot_grpc__messages__pb2.BuildRequest.FromString,
            response_serializer=ryax_dot_action__builder_dot_infrastructure_dot_messaging_dot_messages_dot_grpc__messages__pb2.BuildReply.SerializeToString,
        ),
        "CancelBuild": grpc.unary_unary_rpc_method_handler(
            servicer.CancelBuild,
            request_deserializer=ryax_dot_action__builder_dot_infrastructure_dot_messaging_dot_messages_dot_repository__messages__pb2.ActionCancelBuild.FromString,
            response_serializer=ryax_dot_action__builder_dot_infrastructure_dot_messaging_dot_messages_dot_action__builder__messages__pb2.ActionBuildCanceled.SerializeToString,
        ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
        "grpc_api.ActionBuilder", rpc_method_handlers
    )
    server.add_generic_rpc_handlers((generic_handler,))


# This class is part of an EXPERIMENTAL API.
class ActionBuilder(object):
    """Manage Ryax Action executions"""

    @staticmethod
    def Build(
        request,
        target,
        options=(),
        channel_credentials=None,
        call_credentials=None,
        insecure=False,
        compression=None,
        wait_for_ready=None,
        timeout=None,
        metadata=None,
    ):
        return grpc.experimental.unary_stream(
            request,
            target,
            "/grpc_api.ActionBuilder/Build",
            ryax_dot_action__builder_dot_infrastructure_dot_messaging_dot_messages_dot_grpc__messages__pb2.BuildRequest.SerializeToString,
            ryax_dot_action__builder_dot_infrastructure_dot_messaging_dot_messages_dot_grpc__messages__pb2.BuildReply.FromString,
            options,
            channel_credentials,
            insecure,
            call_credentials,
            compression,
            wait_for_ready,
            timeout,
            metadata,
        )

    @staticmethod
    def CancelBuild(
        request,
        target,
        options=(),
        channel_credentials=None,
        call_credentials=None,
        insecure=False,
        compression=None,
        wait_for_ready=None,
        timeout=None,
        metadata=None,
    ):
        return grpc.experimental.unary_unary(
            request,
            target,
            "/grpc_api.ActionBuilder/CancelBuild",
            ryax_dot_action__builder_dot_infrastructure_dot_messaging_dot_messages_dot_repository__messages__pb2.ActionCancelBuild.SerializeToString,
            ryax_dot_action__builder_dot_infrastructure_dot_messaging_dot_messages_dot_action__builder__messages__pb2.ActionBuildCanceled.FromString,
            options,
            channel_credentials,
            insecure,
            call_credentials,
            compression,
            wait_for_ready,
            timeout,
            metadata,
        )
