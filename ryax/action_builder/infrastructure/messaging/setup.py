# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from ryax.action_builder.container import ApplicationContainer
from ryax.action_builder.infrastructure.messaging.handlers import action_build_handler
from ryax.action_builder.infrastructure.messaging.utils.consumer import (
    MessagingConsumer,
)


def setup(consumer: MessagingConsumer, container: ApplicationContainer) -> None:
    """Method to setup messaging mapper (event type/messages mapping)"""
    container.wire(modules=[action_build_handler])

    # Register message controller
    consumer.register_handler("ActionBuild", action_build_handler.on_action_build)
    consumer.register_handler(
        "ActionCancelBuild", action_build_handler.on_action_build_cancel
    )
    consumer.register_handler("ActionDeleted", action_build_handler.on_action_deleted)
