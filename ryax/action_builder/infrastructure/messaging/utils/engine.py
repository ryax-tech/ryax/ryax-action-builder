# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from logging import Logger, getLogger
from typing import Optional

from aio_pika import connect_robust
from aio_pika.abc import AbstractChannel, AbstractRobustConnection


class MessagingEngine:
    """Class to handle connection to messaging system (RabbitMQ)"""

    def __init__(self, connection_url: str) -> None:
        self.connection_url: str = connection_url
        self.connection: Optional[AbstractRobustConnection] = None
        self.logger: Logger = getLogger(self.__class__.__name__)

    async def connect(self) -> None:
        """Method to connect to connect to messaging system"""
        self.logger.info("Connecting to broker.")
        self.connection = await connect_robust(self.connection_url)

    async def disconnect(self) -> None:
        """Method to connect to connect to messaging system"""
        self.logger.info("Disconnecting from broker")
        if self.connection is not None:
            await self.connection.close()

    async def get_channel(self) -> AbstractChannel:
        """Method to get a channel from active connection"""
        if self.connection is not None:
            return await self.connection.channel()
        raise Exception("Call connect() first")
