# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from logging import Logger, getLogger
from typing import List, Union

from aio_pika import ExchangeType, Message
from google.protobuf.struct_pb2 import Struct

from ryax.action_builder.domain.action_build.action_build_events import (
    ActionBuildEvent,
    ActionBuildErrorEvent,
    ActionBuildStartEvent,
    ActionBuildSuccessEvent,
    ActionReadyEvent,
    ActionDeletedEvent,
    ActionBuildCanceledEvent,
    NoActionBuildEvent,
)
from ryax.action_builder.domain.events.base_event import BaseEvent
from ryax.action_builder.domain.events.event_publisher import IEventPublisher
from ryax.action_builder.infrastructure.messaging.messages.action_builder_messages_pb2 import (
    ActionReady,
    ActionBuildError,
    ActionBuildSuccess,
    ActionBuildStart,
    ActionBuildCanceled,
    NoActionBuild,
)
from ryax.action_builder.infrastructure.messaging.utils.engine import MessagingEngine

MessageEvent = Union[
    ActionBuildStart,
    ActionBuildSuccess,
    ActionBuildError,
    ActionBuildCanceled,
    ActionReady,
    NoActionBuild,
]

logger: Logger = getLogger(__name__)


class MessagingPublisher(IEventPublisher):
    def __init__(self, engine: MessagingEngine):
        self.engine: MessagingEngine = engine

    @staticmethod
    def _handle_action_ready_event(event: ActionReadyEvent) -> ActionReady:
        action_ready = ActionReady()
        action_ready.lockfile = event.lockfile
        action_ready.action_id = event.action_id
        action_ready.action_name = event.action_name
        action_ready.action_technical_name = event.action_technical_name
        action_ready.action_version = event.action_version
        action_ready.action_kind = ActionReady.Kind.Value(event.action_kind.name)
        action_ready.action_build_date.FromDatetime(event.action_build_date)
        action_ready.action_owner_id = event.action_owner_id
        action_ready.action_project_id = event.action_project_id
        if event.action_description is not None:
            action_ready.action_description = event.action_description
        action_ready.action_dynamic_outputs = event.action_dynamic_outputs
        action_ready.action_owner_id = event.action_owner_id
        action_ready.action_categories.extend(event.action_categories)
        action_ready.action_project_id = event.action_project_id
        for addon_name, addon_spec in event.action_addons.items():
            action_ready.action_addons.get_or_create(addon_name)
            value_struct = Struct()
            value_struct.update(addon_spec)
            action_ready.action_addons[addon_name].CopyFrom(value_struct)

        for item in event.action_inputs:
            action_ready_input = ActionReady.ActionIO()
            action_ready_input.id = item.id
            action_ready_input.technical_name = item.technical_name
            action_ready_input.display_name = item.display_name
            action_ready_input.help = item.help
            action_ready_input.type = ActionReady.ActionIO.Type.Value(item.type.name)
            action_ready_input.enum_values.extend(item.enum_values)
            if item.default_value is not None:
                action_ready_input.default_value = item.default_value
            action_ready_input.optional = item.optional
            action_ready.action_inputs.append(action_ready_input)

        for item in event.action_outputs:
            action_ready_output = ActionReady.ActionIO()
            action_ready_output.id = item.id
            action_ready_output.technical_name = item.technical_name
            action_ready_output.display_name = item.display_name
            action_ready_output.help = item.help
            action_ready_output.type = ActionReady.ActionIO.Type.Value(item.type.name)
            action_ready_output.enum_values.extend(item.enum_values)
            action_ready_output.optional = item.optional
            action_ready.action_outputs.append(action_ready_output)

        if event.action_logo:
            action_ready.action_logo.action_logo_id = event.action_logo.action_logo_id
            action_ready.action_logo.action_logo_name = (
                event.action_logo.action_logo_name
            )
            action_ready.action_logo.action_logo_extension = (
                event.action_logo.action_logo_extension
            )
            assert event.action_logo.action_logo_content is not None
            action_ready.action_logo.action_logo_content = (
                event.action_logo.action_logo_content
            )

        if event.action_resources is not None:
            if event.action_resources.cpu is not None:
                action_ready.action_resources.cpu = event.action_resources.cpu
            if event.action_resources.memory is not None:
                action_ready.action_resources.memory = event.action_resources.memory
            if event.action_resources.time is not None:
                action_ready.action_resources.time = event.action_resources.time
            if event.action_resources.gpu is not None:
                action_ready.action_resources.gpu = event.action_resources.gpu
        logger.debug(f"Sending Message Content: {action_ready}")
        return action_ready

    def handle_event(self, event: BaseEvent) -> MessageEvent:
        if isinstance(event, ActionBuildStartEvent):
            return ActionBuildStart(action_id=event.action_id)
        elif isinstance(event, ActionBuildSuccessEvent):
            return ActionBuildSuccess(action_id=event.action_id)
        elif isinstance(event, ActionReadyEvent):
            return self._handle_action_ready_event(event)
        elif isinstance(event, ActionBuildCanceledEvent):
            return ActionBuildCanceled(
                action_id=event.action_id,
            )
        elif isinstance(event, ActionBuildErrorEvent):
            return ActionBuildError(
                action_id=event.action_id,
                logs=event.logs,
                code=ActionBuildError.ErrorCodes.ValueType(event.code),
            )
        elif isinstance(event, NoActionBuildEvent):
            return NoActionBuild()
        else:
            raise Exception("Event is serializable into message: %s" % event)

    async def publish(self, events: List[BaseEvent]) -> None:
        logger.info("Start message publisher")
        channel = await self.engine.get_channel()
        exchange = await channel.declare_exchange(
            "domain_events", ExchangeType.TOPIC, durable=True
        )
        for event in events:
            message_content = self.handle_event(event)

            if isinstance(event, ActionBuildEvent):
                event_type = "ActionBuild"
            elif isinstance(event, ActionBuildStartEvent):
                event_type = "ActionBuildStart"
            elif isinstance(event, ActionBuildSuccessEvent):
                event_type = "ActionBuildSuccess"
            elif isinstance(event, ActionBuildErrorEvent):
                event_type = "ActionBuildError"
            elif isinstance(event, ActionDeletedEvent):
                event_type = "ActionDelete"
            elif isinstance(event, ActionBuildCanceledEvent):
                event_type = "ActionBuildCanceled"
            elif isinstance(event, NoActionBuildEvent):
                event_type = "NoActionBuild"
            elif isinstance(event, ActionReadyEvent):
                event_type = "ActionReady"
            else:
                raise Exception(f"Unknown event {event}")

            logger.debug("publish an event %s: %s", event, event_type)
            message = Message(type=event_type, body=message_content.SerializeToString())
            await exchange.publish(message, routing_key=f"ActionBuilder.{event_type}")
        await channel.close()
