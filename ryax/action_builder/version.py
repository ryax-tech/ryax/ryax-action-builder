# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import importlib.metadata


try:
    __version__ = importlib.metadata.version(__package__)
except NameError:
    __version__ = "dev"
