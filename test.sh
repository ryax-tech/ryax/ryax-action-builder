#!/usr/bin/env bash

set -e

# Override launcher version
# export RYAX_LAUNCHER_IMAGE=registry.ryax.org/ryax-launcher:master

export RYAX_INTERNAL_REGISTRY="localhost:5000"
export RYAX_MAIN_DIR=../.
export PYTHONPATH=./.

clean_up() {
    set +e
    echo "Cleaning testing environment..."
    rm -rf $RYAX_DATA_DIR
    docker compose down -v
}
trap clean_up EXIT

docker compose up -d

pytest -ra $@
