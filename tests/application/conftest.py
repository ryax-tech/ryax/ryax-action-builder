from unittest import mock

import pytest

from ryax.action_builder.container import ApplicationContainer
from ryax.action_builder.domain.action_build.action_build_factory import (
    ActionBuildFactory,
)
from ryax.action_builder.domain.action_build.action_build_processor import (
    IActionBuildProcessor,
)
from ryax.action_builder.domain.events.event_publisher import IEventPublisher


@pytest.fixture(scope="function")
def app_container():
    app_container = ApplicationContainer()
    action_build_processor = mock.MagicMock(IActionBuildProcessor)
    app_container.builder_service.override(action_build_processor)
    messaging_publisher = mock.MagicMock(IEventPublisher)
    app_container.messaging_publisher.override(messaging_publisher)
    action_build_factory = mock.MagicMock(ActionBuildFactory)
    app_container.action_build_factory.override(action_build_factory)

    return app_container
