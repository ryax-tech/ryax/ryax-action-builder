from datetime import datetime
from unittest import mock
from unittest.mock import AsyncMock

from ryax.action_builder.application.action_build_locksync import (
    ActionBuildLockSyncService,
)
from ryax.action_builder.application.action_build_service import ActionBuildService
from ryax.action_builder.container import ApplicationContainer
from ryax.action_builder.domain.action_build.action_build import ActionBuild
from ryax.action_builder.domain.action_build.action_build_events import (
    ActionBuildErrorEvent,
    ActionBuildEvent,
    ActionBuildStartEvent,
    ActionBuildSuccessEvent,
    ActionDeletedEvent,
    ActionBuildCanceledEvent,
    ActionReadyEvent,
)
from ryax.action_builder.domain.action_build.action_build_exceptions import (
    BuildErrorCode,
    BuildException,
)
from ryax.action_builder.domain.action_build.action_build_factory import (
    ActionBuildFactory,
)
from ryax.action_builder.domain.action_build.action_build_processor import (
    IActionBuildProcessor,
)
from ryax.action_builder.domain.action_build.action_build_values import (
    ActionKind,
    ActionType,
)
from ryax.action_builder.domain.events.event_publisher import IEventPublisher
from ryax.action_builder.infrastructure.builder.builder_service import BuilderService
from ryax.action_builder.infrastructure.builder.external_commands import (
    ExternalCommandsService,
)


async def test_process_action_build(app_container: ApplicationContainer) -> None:
    action_id = "action_id"
    action_build_event = ActionBuildEvent(
        action_id=action_id,
        action_type=ActionType.Python3,
        action_kind=ActionKind.PROCESSOR,
        action_version="12.3",
        action_project="project",
        action_owner_id="Dadou",
        action_archive=b"archive bytes content",
        action_technical_name="action_technical_name",
        action_wrapper_type_list=[],
        action_revision="",
    )
    build_start_event = ActionBuildStartEvent(action_id=action_id)
    build_success_event = ActionBuildSuccessEvent(action_id=action_id)
    action_build = ActionBuild(
        action_id=action_id,
        version="1.0",
        type=ActionType.Python3,
        kind=ActionKind.PROCESSOR,
        archive=b"archive bytes content",
        technical_name="action_technical_name",
        wrapper_type_list=[],
        project="project",
        owner_id="Dadou",
    )

    action_ready_event = ActionReadyEvent(
        action_id="test id",
        action_name="Mock Action",
        action_technical_name="mock_action",
        action_version="1.0.0",
        action_project_id="project id",
        action_owner_id="owner id",
        action_build_date=datetime.now(),
        action_creation_date=datetime.now(),
        action_description="Mock action for testing.",
        action_type=None,  # Replace with actual RepositoryActionType if needed
        action_kind=None,  # Replace with actual RepositoryActionKind if needed
        action_resources=None,  # Optional, so left empty
        action_dynamic_outputs=False,
        action_inputs=[],  # No inputs for this test
        action_outputs=[],  # No outputs for this test
        action_categories=[],  # No categories for this test
        action_logo=None,  # Optional, so left empty
        action_addons={},  # Empty dictionary for addons
        lockfile=b"",
    )

    messaging_publisher: mock.MagicMock[
        IEventPublisher
    ] = app_container.messaging_publisher()
    messaging_publisher.publish = mock.AsyncMock()

    action_build_factory: mock.MagicMock[
        ActionBuildFactory
    ] = app_container.action_build_factory()
    action_build_factory.from_action_build_event = mock.MagicMock(
        return_value=action_build
    )
    builder_service: mock.MagicMock[
        IActionBuildProcessor
    ] = app_container.builder_service()
    builder_service.process_build = mock.AsyncMock(
        return_value=(None, action_ready_event)
    )
    locksync_service: mock.MagicMock[
        ActionBuildLockSyncService
    ] = app_container.action_build_locksync()
    locksync_service.unlock_build = mock.MagicMock()

    action_build_service: ActionBuildService = (
        app_container.action_build_service_broker()
    )
    await action_build_service.process_action_build(action_build_event)

    builder_service.process_build.assert_called_once_with(action_build)

    locksync_service.unlock_build.assert_called_once()

    messaging_publisher.publish.assert_has_calls(
        [
            mock.call([build_start_event]),
            mock.call([action_ready_event]),
            mock.call([build_success_event]),
        ]
    )


async def test_process_action_build_when_fail(
    app_container: ApplicationContainer,
) -> None:
    action_id = "action_id"
    action_build_event = ActionBuildEvent(
        action_id=action_id,
        action_type=ActionType.Python3,
        action_project="project",
        action_owner_id="Dadou",
        action_kind=ActionKind.PROCESSOR,
        action_version="12.3",
        action_archive=b"archive bytes content",
        action_technical_name="action_technical_name",
        action_wrapper_type_list=[],
        action_revision="",
    )
    build_start_event = ActionBuildStartEvent(action_id=action_id)
    action_build = ActionBuild(
        action_id=action_id,
        version="1.0",
        type=ActionType.Python3,
        kind=ActionKind.PROCESSOR,
        archive=b"archive bytes content",
        technical_name="action_technical_name",
        project="project",
        owner_id="Dadou",
        wrapper_type_list=[],
    )
    build_error = BuildException(
        code=BuildErrorCode.ErrorWhileRunningProcess, logs="build error"
    )
    build_error_event = ActionBuildErrorEvent(
        action_id=action_id, code=build_error.code.value, logs=build_error.logs
    )
    messaging_publisher: mock.MagicMock[
        IEventPublisher
    ] = app_container.messaging_publisher()
    messaging_publisher.publish = mock.AsyncMock()
    action_build_factory: mock.MagicMock[
        ActionBuildFactory
    ] = app_container.action_build_factory()
    action_build_factory.from_action_build_event = mock.MagicMock(
        return_value=action_build
    )
    builder_service: mock.MagicMock[
        IActionBuildProcessor
    ] = app_container.builder_service()
    builder_service.process_build = mock.AsyncMock(side_effect=build_error)

    locksync_service: mock.MagicMock[
        ActionBuildLockSyncService
    ] = app_container.action_build_locksync()
    locksync_service.unlock_build = mock.MagicMock()

    action_build_service: ActionBuildService = (
        app_container.action_build_service_broker()
    )
    await action_build_service.process_action_build(action_build_event)

    locksync_service.unlock_build.assert_called_once()

    action_build_factory.from_action_build_event.assert_called_once_with(
        action_build_event
    )
    builder_service.process_build.assert_called_once_with(action_build)
    print(messaging_publisher.publish)
    messaging_publisher.publish.assert_has_calls(
        [
            mock.call([build_start_event]),
            mock.call([build_error_event]),
        ]
    )


async def test_delete_action(app_container: ApplicationContainer) -> None:
    action_id = "action_id"
    action_version = "1.2"
    action_deleted_event = ActionDeletedEvent(
        action_id=action_id, action_version=action_version
    )
    builder_service: mock.MagicMock[
        IActionBuildProcessor
    ] = app_container.builder_service()
    builder_service.cleanup = mock.AsyncMock()
    action_build_service: ActionBuildService = (
        app_container.action_build_service_broker()
    )
    await action_build_service.delete_action(action_deleted_event)
    builder_service.cleanup.assert_called_once_with(action_id, action_version)


async def test_process_action_build_when_cancel(
    app_container: ApplicationContainer,
) -> None:
    action_id = "action_id"
    action_build_event = ActionBuildEvent(
        action_id=action_id,
        action_type=ActionType.Python3,
        action_kind=ActionKind.PROCESSOR,
        action_version="12.3",
        action_archive=b"archive bytes content",
        action_technical_name="action_technical_name",
        action_wrapper_type_list=[],
        action_project="project",
        action_owner_id="Dadou",
        action_revision="",
    )
    build_start_event = ActionBuildStartEvent(action_id=action_id)
    action_build = ActionBuild(
        action_id=action_id,
        version="1.0",
        type=ActionType.Python3,
        kind=ActionKind.PROCESSOR,
        archive=b"archive bytes content",
        technical_name="action_technical_name",
        wrapper_type_list=[],
        project="project",
        owner_id="Dadou",
    )
    build_error = BuildException(code=BuildErrorCode.BuildCanceled, logs="build error")
    build_error_event = ActionBuildCanceledEvent(action_id=action_id)
    messaging_publisher: mock.MagicMock[
        IEventPublisher
    ] = app_container.messaging_publisher()
    messaging_publisher.publish = mock.AsyncMock()
    action_build_factory: mock.MagicMock[
        ActionBuildFactory
    ] = app_container.action_build_factory()
    action_build_factory.from_action_build_event = mock.MagicMock(
        return_value=action_build
    )
    builder_service: mock.MagicMock[
        IActionBuildProcessor
    ] = app_container.builder_service()
    locksync_service: mock.MagicMock[
        ActionBuildLockSyncService
    ] = app_container.action_build_locksync()
    locksync_service.unlock_build = mock.MagicMock()

    builder_service.process_build = mock.AsyncMock(side_effect=build_error)
    action_build_service: ActionBuildService = (
        app_container.action_build_service_broker()
    )
    await action_build_service.process_action_build(action_build_event)

    locksync_service.unlock_build.assert_called_once()
    action_build_factory.from_action_build_event.assert_called_once_with(
        action_build_event
    )
    builder_service.process_build.assert_called_once_with(action_build)
    messaging_publisher.publish.assert_has_calls(
        [
            mock.call([build_start_event]),
            mock.call([build_error_event]),
        ]
    )


async def test_gc_triggered():
    mocked_ext_command_service = mock.AsyncMock(ExternalCommandsService)
    mocked_ext_command_service.run_nix_garbage_collection = AsyncMock()
    # Only 20% of the disk is free
    mocked_ext_command_service.get_nix_store_stats.return_value = 2, 10
    builder_service = BuilderService(mocked_ext_command_service, "fake_registry")
    await builder_service.garbage_collect(0.7)
    mocked_ext_command_service.run_nix_garbage_collection.assert_awaited_once()


async def test_gc_not_triggered():
    mocked_ext_command_service = mock.AsyncMock(ExternalCommandsService)
    # Only 20% of the disk is free
    mocked_ext_command_service.get_nix_store_stats.return_value = 2, 10
    builder_service = BuilderService(mocked_ext_command_service, "fake_registry")
    await builder_service.garbage_collect(0.9)
    mocked_ext_command_service.run_nix_garbage_collection.assert_not_awaited()
