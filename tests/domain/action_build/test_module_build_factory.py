import pytest

from ryax.action_builder.container import ApplicationContainer
from ryax.action_builder.domain.action_build.action_build import ActionBuild
from ryax.action_builder.domain.action_build.action_build_events import ActionBuildEvent
from ryax.action_builder.domain.action_build.action_build_factory import (
    ActionBuildFactory,
)
from ryax.action_builder.domain.action_build.action_build_values import (
    ActionKind,
    ActionType,
)


@pytest.fixture(scope="function")
def app_container():
    container = ApplicationContainer()
    yield container


def test_from_action_build_event(app_container: ApplicationContainer) -> None:
    event = ActionBuildEvent(
        action_id="action_id",
        action_type=ActionType.Python3,
        action_kind=ActionKind.PROCESSOR,
        action_version="12.3",
        action_project="project",
        action_owner_id="Dadou",
        action_archive=b"archive bytes content",
        action_technical_name="action_technical_name",
        action_wrapper_type_list=[],
        action_revision="",
    )
    action_build_factory: ActionBuildFactory = app_container.action_build_factory()
    assert action_build_factory.from_action_build_event(event) == ActionBuild(
        action_id=event.action_id,
        type=ActionType.Python3,
        kind=ActionKind.PROCESSOR,
        version=event.action_version,
        archive=event.action_archive,
        project="project",
        owner_id="Dadou",
        technical_name="action_technical_name",
        wrapper_type_list=[],
    )
