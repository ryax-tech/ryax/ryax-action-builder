from unittest import mock

import pytest
from aiohttp import web

from ryax.action_builder.application.util_service import UtilService
from ryax.action_builder.container import ApplicationContainer
from ryax.action_builder.infrastructure.api.setup import setup as setup_api


@pytest.fixture()
def app_container():
    app_container = ApplicationContainer()
    yield app_container
    app_container.unwire()


@pytest.fixture()
async def api_client(app_container: ApplicationContainer, aiohttp_client):
    """Instantiates client to test api part"""
    app = web.Application()
    setup_api(app, app_container)
    return await aiohttp_client(app)


@pytest.fixture()
def util_service_mock(app_container: ApplicationContainer):
    util_service_mock = mock.MagicMock(UtilService)
    app_container.util_service.override(util_service_mock)

    return util_service_mock
