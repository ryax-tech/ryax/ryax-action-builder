import pytest

from ryax.action_builder.container import ApplicationContainer


@pytest.fixture(scope="function")
def app_container():
    app_container = ApplicationContainer()
    return app_container
