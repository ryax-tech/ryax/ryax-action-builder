from ryax.action_builder.container import ApplicationContainer
from ryax.action_builder.domain.action_build.action_build_events import (
    ActionBuildErrorEvent,
)
from ryax.action_builder.infrastructure.messaging.messages.action_builder_messages_pb2 import (
    ActionBuildError,
)
from ryax.action_builder.infrastructure.messaging.utils.publisher import (
    MessagingPublisher,
)


def test_handle_event(app_container: ApplicationContainer) -> None:
    action_build_error_event = ActionBuildErrorEvent(
        action_id="abc123",
        logs="logs_message",
        code=2,
    )
    messaging_publisher: MessagingPublisher = app_container.messaging_publisher()
    result = messaging_publisher.handle_event(action_build_error_event)
    assert isinstance(result, ActionBuildError)
    assert result.action_id == action_build_error_event.action_id
    assert result.logs == action_build_error_event.logs
    assert result.code == action_build_error_event.code
